-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               10.1.9-MariaDB-log - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- Dumping data for table mgi.cms_category: 0 rows
/*!40000 ALTER TABLE `cms_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_category` ENABLE KEYS */;

-- Dumping data for table mgi.cms_category_lang: 0 rows
/*!40000 ALTER TABLE `cms_category_lang` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_category_lang` ENABLE KEYS */;

-- Dumping data for table mgi.cms_config: ~6 rows (approximately)
/*!40000 ALTER TABLE `cms_config` DISABLE KEYS */;
INSERT INTO `cms_config` (`id`, `name`, `value`, `label`) VALUES
	(1, 'site_name', 'Garapic Inc.', 'Name of Applications'),
	(2, 'site_url', 'http://garapic.dev', 'Base URL'),
	(3, 'version', '1.0', 'Version'),
	(4, 'perpage', '10', 'Max. Data Per Page'),
	(5, 'date_format', 'Y-m-d h:i:s A', 'Date format'),
	(6, 'language', 'en', 'Default Language'),
	(7, 'site_name', 'IGI', NULL),
	(8, 'site_tag', 'Ikatan Geografi Indonesia', NULL),
	(9, 'CP1', 'Ibu Irma - 08129 446 330', NULL),
	(10, 'Email', 'pitigi2015@gmail.com', NULL),
	(11, 'Judul Pendaftaran', 'Saya ingin <span>mendaftarkan diri</span> menjadi anggota IGI', NULL),
	(12, 'Info Pendaftaran', 'Ketentuan dan persyaratan akan dijelaskan pada laman pendaftaran', NULL),
	(13, 'Alamat 1', 'Bulaksumur, Jl. Kaliurang', NULL),
	(14, 'Alamat 2', 'Yogyakarta 55281 Indonesia', NULL),
	(15, 'Facebook', 'seminar.igi', NULL),
	(16, 'CP2', 'Bapak Samadi - 08963 2030 939', NULL),
	(17, 'Telepon', '(0274) 6492340', NULL),
	(18, 'Fax', '(0274) 589595', NULL);
/*!40000 ALTER TABLE `cms_config` ENABLE KEYS */;

-- Dumping data for table mgi.cms_event: 0 rows
/*!40000 ALTER TABLE `cms_event` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_event` ENABLE KEYS */;

-- Dumping data for table mgi.cms_event_lang: 0 rows
/*!40000 ALTER TABLE `cms_event_lang` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_event_lang` ENABLE KEYS */;

-- Dumping data for table mgi.cms_groups: ~1 rows (approximately)
/*!40000 ALTER TABLE `cms_groups` DISABLE KEYS */;
INSERT INTO `cms_groups` (`id`, `name`, `description`) VALUES
	(0, 'Administrator', 'Highest level of users privillegges. Do not remove.');
/*!40000 ALTER TABLE `cms_groups` ENABLE KEYS */;

-- Dumping data for table mgi.cms_lang: 2 rows
/*!40000 ALTER TABLE `cms_lang` DISABLE KEYS */;
INSERT INTO `cms_lang` (`id`, `shortname`, `name`, `is_default`, `last_update`) VALUES
	(1, 'en', 'English', 1, '2016-05-02 07:42:15'),
	(2, 'id', 'Bahasa Indonesia', 0, '2016-05-02 07:42:24');
/*!40000 ALTER TABLE `cms_lang` ENABLE KEYS */;

-- Dumping data for table mgi.cms_log: 0 rows
/*!40000 ALTER TABLE `cms_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_log` ENABLE KEYS */;

-- Dumping data for table mgi.cms_media: 0 rows
/*!40000 ALTER TABLE `cms_media` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_media` ENABLE KEYS */;

-- Dumping data for table mgi.cms_media_lang: 0 rows
/*!40000 ALTER TABLE `cms_media_lang` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_media_lang` ENABLE KEYS */;

-- Dumping data for table mgi.cms_member: 0 rows
/*!40000 ALTER TABLE `cms_member` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_member` ENABLE KEYS */;

-- Dumping data for table mgi.cms_member_abdi: 0 rows
/*!40000 ALTER TABLE `cms_member_abdi` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_member_abdi` ENABLE KEYS */;

-- Dumping data for table mgi.cms_member_edu: 0 rows
/*!40000 ALTER TABLE `cms_member_edu` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_member_edu` ENABLE KEYS */;

-- Dumping data for table mgi.cms_member_jabatan: 0 rows
/*!40000 ALTER TABLE `cms_member_jabatan` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_member_jabatan` ENABLE KEYS */;

-- Dumping data for table mgi.cms_member_office: 0 rows
/*!40000 ALTER TABLE `cms_member_office` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_member_office` ENABLE KEYS */;

-- Dumping data for table mgi.cms_member_publikasi: 0 rows
/*!40000 ALTER TABLE `cms_member_publikasi` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_member_publikasi` ENABLE KEYS */;

-- Dumping data for table mgi.cms_member_riset: 0 rows
/*!40000 ALTER TABLE `cms_member_riset` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_member_riset` ENABLE KEYS */;

-- Dumping data for table mgi.cms_member_tokens: 0 rows
/*!40000 ALTER TABLE `cms_member_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_member_tokens` ENABLE KEYS */;

-- Dumping data for table mgi.cms_modules: 20 rows
/*!40000 ALTER TABLE `cms_modules` DISABLE KEYS */;
INSERT INTO `cms_modules` (`id`, `name`, `parent`, `show`, `path`, `icon`, `has_write`, `has_update`, `has_delete`, `orders`) VALUES
	(1, 'Dashboard', 0, 1, 'dashboard', 'fa-home', 0, 0, 0, 1),
	(2, 'Systems', 0, 1, '#', 'fa-gears', 0, 0, 0, 5),
	(3, 'Modules', 2, 1, 'packages', 'fa-folder-open', 1, 1, 1, 2),
	(4, 'Priviledges', 8, 1, 'priviledges', 'fa-lock', 1, 1, 1, 2),
	(5, 'System', 2, 1, 'general', 'fa-desktop', 1, 1, 1, 1),
	(6, 'Groups', 8, 1, 'groups', 'fa-sitemap', 1, 1, 1, 1),
	(7, 'Logs System', 2, 1, 'logs', 'fa-rocket', 0, 0, 0, 3),
	(8, 'Users', 0, 1, '#', 'fa-users', 1, 1, 1, 4),
	(9, 'List', 8, 1, 'auth', '', 1, 1, 1, 4),
	(10, 'Content', 0, 1, '#', 'fa-list', 1, 1, 1, 2),
	(11, 'Blogs', 10, 1, 'blogs', '', 1, 1, 1, 0),
	(12, 'Pages', 10, 1, 'pages', '', 1, 1, 1, 6),
	(13, 'Category', 10, 1, 'category', '', 1, 1, 1, 6),
	(14, 'Media', 0, 1, '#', 'fa-folder', 1, 1, 1, 3),
	(15, 'Folders', 14, 1, 'folders', '', 1, 1, 1, 0),
	(16, 'Files', 14, 1, 'files', '', 1, 1, 1, 6),
	(18, 'Events', 10, 1, 'events', '', 1, 1, 1, 6),
	(19, 'Frontpage', 0, 1, '../', 'fa-desktop', 1, 1, 1, 0),
	(20, 'Navigations', 10, 1, 'nav', '', 1, 1, 1, 0),
	(21, 'Members', 0, 0, 'members', '', 1, 1, 1, 7);
/*!40000 ALTER TABLE `cms_modules` ENABLE KEYS */;

-- Dumping data for table mgi.cms_nav: 0 rows
/*!40000 ALTER TABLE `cms_nav` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_nav` ENABLE KEYS */;

-- Dumping data for table mgi.cms_nav_groups: ~0 rows (approximately)
/*!40000 ALTER TABLE `cms_nav_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_nav_groups` ENABLE KEYS */;

-- Dumping data for table mgi.cms_page: 0 rows
/*!40000 ALTER TABLE `cms_page` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_page` ENABLE KEYS */;

-- Dumping data for table mgi.cms_page_lang: 0 rows
/*!40000 ALTER TABLE `cms_page_lang` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_page_lang` ENABLE KEYS */;

-- Dumping data for table mgi.cms_priviledges: 20 rows
/*!40000 ALTER TABLE `cms_priviledges` DISABLE KEYS */;
INSERT INTO `cms_priviledges` (`modules_id`, `groups_id`, `can_write`, `can_update`, `can_delete`, `created`, `created_by`) VALUES
	(19, 0, 1, 1, 1, '2016-05-11 16:49:34', 1),
	(16, 0, 1, 1, 1, '2016-05-11 16:49:34', 1),
	(15, 0, 1, 1, 1, '2016-05-11 16:49:34', 1),
	(14, 0, 1, 1, 1, '2016-05-11 16:49:34', 1),
	(20, 0, 1, 1, 1, '2016-05-11 16:49:34', 1),
	(18, 0, 1, 1, 1, '2016-05-11 16:49:34', 1),
	(13, 0, 1, 1, 1, '2016-05-11 16:49:34', 1),
	(12, 0, 1, 1, 1, '2016-05-11 16:49:34', 1),
	(11, 0, 1, 1, 1, '2016-05-11 16:49:34', 1),
	(10, 0, 1, 1, 1, '2016-05-11 16:49:34', 1),
	(9, 0, 1, 1, 1, '2016-05-11 16:49:34', 1),
	(6, 0, 1, 1, 1, '2016-05-11 16:49:34', 1),
	(4, 0, 1, 1, 1, '2016-05-11 16:49:34', 1),
	(8, 0, 1, 1, 1, '2016-05-11 16:49:34', 1),
	(7, 0, 0, 0, 0, '2016-05-11 16:49:34', 1),
	(5, 0, 1, 1, 1, '2016-05-11 16:49:34', 1),
	(3, 0, 1, 1, 1, '2016-05-11 16:49:34', 1),
	(2, 0, 0, 0, 0, '2016-05-11 16:49:34', 1),
	(1, 0, 0, 0, 0, '2016-05-11 16:49:34', 1),
	(21, 0, 1, 1, 1, '2016-05-11 16:49:34', 1);
/*!40000 ALTER TABLE `cms_priviledges` ENABLE KEYS */;

-- Dumping data for table mgi.cms_users: ~2 rows (approximately)
/*!40000 ALTER TABLE `cms_users` DISABLE KEYS */;
INSERT INTO `cms_users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
	(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1463032092, 1, 'Eddy', 'Subratha', 'Admin', '321'),
	(2, '::1', NULL, '$2y$08$kfwzxLwzFIIDv3.Rw5LySOxSMo7NkPzxa1Rt9vJSj7JMCKAYamR2m', NULL, 'eddy.subratha@gmail.com', NULL, NULL, NULL, NULL, 1456843873, 1457692478, 1, 'Eddy', 'Nugoroh', 'Accounting Dept.', '085228720780');
/*!40000 ALTER TABLE `cms_users` ENABLE KEYS */;

-- Dumping data for table mgi.cms_users_groups: ~2 rows (approximately)
/*!40000 ALTER TABLE `cms_users_groups` DISABLE KEYS */;
INSERT INTO `cms_users_groups` (`id`, `user_id`, `group_id`) VALUES
	(30, 1, 0),
	(26, 2, 1);
/*!40000 ALTER TABLE `cms_users_groups` ENABLE KEYS */;

-- Dumping data for table mgi.cms_stats: 0 rows
/*!40000 ALTER TABLE `cms_stats` DISABLE KEYS */;
INSERT INTO `cms_stats` (`sessid`, `updated_at`) VALUES
	('f02ee30acf23cefeacd5580f4be101fa799', '2016-05-12 14:13:25'),
	('e4a7bfdb8cf5794f3431ac892f784cd7fe4', '2016-05-12 14:30:08'),
	('a8813f18e044366455b1a4d2dd60882fe2e', '2016-05-12 14:35:18'),
	('57055730f78ceefedd8b64599ae97b6f472', '2016-05-12 14:40:32');
/*!40000 ALTER TABLE `cms_stats` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

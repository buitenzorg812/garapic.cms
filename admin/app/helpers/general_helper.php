<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('get_conf'))
{
	function get_conf($name = '')
	{
		$CI 		=& get_instance();
		$sql 		= "SELECT value FROM setting WHERE name='$name'";
		$query 		= $CI->db->query($sql);	 
		if ($query->num_rows() > 0) {
			$setting	= $query->row();
			return $setting->value;
		}
	}
}

if ( ! function_exists('get_remote_url'))
{
	function get_remote_url()
	{
		// $remote_url 		= 'http://128.199.70.250/';
		$remote_url 		= 'http://localhost/apps/mgi';
		return $remote_url;
	}
}

if ( ! function_exists('get_remote_path'))
{
	function get_remote_path()
	{
		$resource_path 		= MEDIA;
		return get_remote_url().$resource_path;
	}
}

if ( ! function_exists('get_user_counter'))
{
	function get_user_counter()
	{
		// Define CI
		$CI 		=& get_instance();
		$count 		= $CI->db->count_all('user_online');

		if($count > 0) {
			return $count; 
		} else { 
			return 0; 
		} 
	}
}

if ( ! function_exists('get_user_online'))
{
	function get_user_online()
	{
		// Define CI
		$CI 		=& get_instance();

		/* Define how long the maximum amount of time the session can be inactive. */ 
		define("MAX_IDLE_TIME", 3); 

		if ( $directory_handle = opendir( session_save_path() ) ) { 
			$count = 0; 
			while ( false !== ( $file = readdir( $directory_handle ) ) ) { 
				if($file != '.' && $file != '..'){ 
					// Comment the 'if(...){' and '}' lines if you get a significant amount of traffic 
					if(time()- fileatime(session_save_path() . '/' . $file) < MAX_IDLE_TIME * 60) { 
						$count++; 
					} 
				} 
			}
			$sql = "INSERT IGNORE INTO user_online(`sessid`) VALUES ('".session_id()."')  ";			
			$CI->db->query($sql);	 
			closedir($directory_handle); 
			return $count; 
		} else { 
			return false; 
		} 
	}
}

function debug($string) 
{
	echo '<pre>';
	print_r($string);
	echo '</pre>';
	exit;
}

function ubah_format_tanggal($tanggal) {
    return date('d-m-Y', strtotime($tanggal));
}

function write_log($log_type, $log_module, $log_msg) {
    $CI = &get_instance();
    $data['log_flag'] = $log_type;
    $data['log_module'] = $log_module;
    $data['log_message'] = $log_msg;
    $data['input_date'] = date('Y-m-d H:i:s');
    $CI->db->insert('syslogs', $data);
}

function gen_random_string($max_string = 20) {
    $letters = 'ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456789';
    $len = strlen($letters);
    $letter = $letters[rand(0, $len - 1)];

    $word = '';
    for ($i = 0; $i < $max_string; $i++) {
        $letter = $letters[rand(0, $len - 1)];
        $word .= $letter;
    }

    return $word;
}
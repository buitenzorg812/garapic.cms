<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MX_Controller {

    private $data = array();

    function __construct()
    {
        parent::__construct();

        // for practice only
        // $this->faker    = Faker\Factory::create();
        // $this->load->model('user_model');
        // $this->load->library('gridtable');

        // title
        $this->data['title'] = 'Welcome';
    }

    public function index()
    {
        redirect(base_url(),'refresh');
        // echo 'Hello World ' . $this->faker->name();
        // $source = 'http://127.0.0.1/apps/ipc_api/index.php/projects';
        // $config = array(
        //     'header' => array('Name', 'Email', 'Information'),
        //     'fields' => array('name', 'code', 'description'),
        //     'button_link' => '/'
        // );
        // $this->data['content'] = $this->gridtable->json_to_table($source, $config);
        // $this->load->view('admin/content', $this->data);
    }
}

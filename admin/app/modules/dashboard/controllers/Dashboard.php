<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller {

    private $data = array();

    public function __construct()
    {
        parent::__construct();
        $this->data['title']        = 'Dashboard';
        $this->data['title_desc']   = 'Summary';

        // for debug purpose
        $this->output->enable_profiler(false);
    }

    public function index()
    {
        $this->data['css_files'] = array(
            base_url() . PLUGINS. "bootstrap-calendar/css/calendar.min.css"
        );

        $this->data['js_files'] = array(
            base_url() . PLUGINS. "underscore/underscore-min.js",
            base_url() . PLUGINS. "bootstrap-calendar/js/calendar.min.js",
            base_url() . PLUGINS. "Chart.js/src/Chart.Core.js",
            base_url() . PLUGINS. "Chart.js/src/Chart.Line.js",
            base_url() . ADM_JS. "pages/dashboard2.js"
        );

        $this->data['requests'] = 0;
        $this->data['approvals'] = 0;
        $this->data['progress'] = 0;
        $this->data['experts'] = 0;
        $this->data['total'] = $this->data['requests'] + $this->data['approvals'] + $this->data['progress'] ;

        $this->data['content']  = $this->load->view('dashboard/home', $this->data, TRUE);;
        $this->load->view('admin/content', $this->data);  
    }

}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */
<!-- Info boxes -->
<div class="row">
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-gray"><i class="ion ion-ios-albums-outline"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">BLOGS</span>
				<span class="info-box-number"><?php echo $requests ?></span>
			</div><!-- /.info-box-content -->
		</div><!-- /.info-box -->
	</div><!-- /.col -->

	<!-- fix for small devices only -->
	<div class="clearfix visible-sm-block"></div>

	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-gray"><i class="ion ion-ios-checkmark-outline"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">MEMBERS</span>
				<span class="info-box-number"><?php echo $approvals ?></span>
			</div><!-- /.info-box-content -->
		</div><!-- /.info-box -->
	</div><!-- /.col -->

	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-gray"><i class="ion ion-ios-paper-outline"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">COMMENTS</span>
				<span class="info-box-number"><?php echo $progress ?></span>
			</div><!-- /.info-box-content -->
		</div><!-- /.info-box -->
	</div><!-- /.col -->

	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-gray"><i class="ion ion-ios-people-outline"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">VISITOR</span>
				<span class="info-box-number"><?php echo $experts ?></span>
			</div><!-- /.info-box-content -->
		</div><!-- /.info-box -->
	</div><!-- /.col -->

</div><!-- /.row -->

<?php if(count($users) > 0) : ?>
    <div class="box tm-padding">
        <div class="box-body no-padding">            
            <table class="table">
            	<thead>
				<tr>
					<th><?php echo lang('index_fname_th');?></th>
					<th><?php echo lang('index_lname_th');?></th>
					<th><?php echo lang('index_email_th');?></th>
					<th><?php echo lang('index_groups_th');?></th>
					<th><?php echo lang('index_status_th');?></th>
					<th><?php echo lang('index_action_th');?></th>
				</tr>
				</thead>
				<tbody>
				<?php foreach ($users as $user):?>
					<tr>
			            <td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
			            <td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
			            <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
						<td>
							<?php foreach ($user->groups as $group):?>
								<?php echo anchor("/groups/update/".$group->id, htmlspecialchars($group->name,ENT_QUOTES,'UTF-8')) ;?><br />
			                <?php endforeach?>
						</td>
						<td><?php echo ($user->active) ? anchor("/auth/deactivate/".$user->id, lang('index_active_link')) : anchor("/auth/activate/". $user->id, lang('index_inactive_link'));?></td>
						<td><?php echo anchor("/auth/edit_user/".$user->id, 'Edit') ;?></td>
					</tr>
				<?php endforeach;?>
				</tbody>
            </table>
        </div>
        <div class="box-footer tm-padding tm-nopadding-right tm-nopadding-bottom text-right">
        <?php echo @$pagination ?>
        </div>
    </div>
<?php else : ?>
    <?php $this->load->view('admin/blank');?>
<?php endif ?>


<?php if(validation_errors()) : ?>
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Error</strong>
        <hr>
        <?php echo validation_errors(); ?>
    </div>
<?php endif ?>
<form action="<?php echo current_url() ?>" method="POST" class="form-horizontal" role="form">
    <div class="row">

        <div class="col-lg-4">
            <div class="box box-default" >
                <div class="box-header with-border">
                    <h3 class="box-title">Folder</h3>
                </div>
                    <div class="list-group" style="padding: 10px;">            
                        <a href="<?php echo site_url('files') ?>" class="list-group-item tm-noborder <?php echo ($this->uri->total_segments() > 1 ) ? '' : 'active' ?>">All</a>
                        <?php foreach($folder as $fold): ?>
                        <a href="<?php echo site_url('files/folder') .'/'. $fold->slug  ?>" class="list-group-item tm-noborder <?php echo ($this->uri->segment($this->uri->total_segments()) == $fold->slug) ? 'active' :'' ?>"><?php echo $fold->name ?></a>
                        <?php endforeach ?>
                    </div>
            </div>
        </div>

        <div class="col-md-8">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo ($this->uri->total_segments() > 1) ? ucwords($this->uri->segment(3)) : '' ?> Files</h3>
                </div>
                <div class="box-body tm-padding tm-padding-left tab-content" id="files">
                    <?php if(count($files) > 0) : ?>
                    <ul class="mailbox-attachments">
                        <?php foreach($files as $file) : ?>
                            <li>
                                <a href="#" data-id="<?php echo $file->id ?>" class="mailbox-attachment-name">
                                    <span class="mailbox-attachment-icon has-img" style="height: 100px; overflow: hidden;">
                                    <img src="<?php echo base_url() . MEDIA . '/' . $file->filename ?>" alt="Attachment"/>
                                    </span>
                                    <div class="mailbox-attachment-info">
                                        <?php echo (strlen($file->filename) >= 20) ? substr($file->filename, 0, 10) .'...'.substr($file->filename, -8) : $file->filename ?>
                                        <span class="mailbox-attachment-size"><?php echo $file->size ?> KB</span>
                                    </div>
                                </a>
                            </li>
                        <?php endforeach ?>
                    </ul>
                    <?php else :?>
                    <div class="text-center">
                        No Media Available. <a href="<?php echo base_url('files/add') ?>">Upload New Media</a>
                    </div>
                    <?php endif ?>
                </div>
            </div>
            <?php echo $pagination ?>
        </div>

    </div>
</form>

<script>
// $(".pagination a[href!='#']").click(function(e){
//     e.preventDefault();
//     $('.pagination li').each(function(){
//         $(this).removeClass('active');
//     });
//     $(this).parent().addClass('active');
//     $.post($(this).attr('href'), function(data) {
//         $('#files').html(data);
//     })
// })

// $(".pagination a[href='#']").click(function(e){
//     $('.pagination li').each(function(){
//         $(this).removeClass('active');
//     });
//     $(this).parent().addClass('active');
//     return true;
// });

</script>
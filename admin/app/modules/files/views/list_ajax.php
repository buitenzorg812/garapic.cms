
<?php if(count($files) > 0) : ?>
<ul class="mailbox-attachments">
    <?php foreach($files as $file) : ?>
        <li>
            <a href="#" data-id="<?php echo $file->id ?>" class="mailbox-attachment-name">
                <span class="mailbox-attachment-icon has-img" style="height: 100px; overflow: hidden;">
                <img src="<?php echo base_url() . MEDIA . '/' . $file->filename ?>" alt="Attachment"/>
                </span>
                <div class="mailbox-attachment-info">
                    <?php echo (strlen($file->filename) >= 20) ? substr($file->filename, 0, 10) .'...'.substr($file->filename, -8) : $file->filename ?>
                    <span class="mailbox-attachment-size"><?php echo $file->size ?> KB</span>
                </div>
            </a>
        </li>
    <?php endforeach ?>
</ul>
<?php else :?>
<div class="text-center">
    No Media Available. <a href="<?php echo base_url('files/add') ?>">Upload New Media</a>
</div>
<?php endif ?>
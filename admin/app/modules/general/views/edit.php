<?php if(isset($message)) : ?>
	<div class="alert alert-danger">
		<?php echo $message; ?>
	</div>
<?php endif ?>
<?php if(validation_errors()) : ?>
	<div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<strong>Error</strong>
		<hr>
		<?php echo validation_errors(); ?>
	</div>
<?php endif ?>
<form action="<?php echo current_url()?>" method="post" role="form" class="form-horizontal" data-validate="parsley">
	<div class="box">
		<div class="box-body tm-padding">
			<?php foreach($form as $config ) : ?>
				<div class="form-group col-md-4">
					<label for="site_name"><?php echo $config->label?></label>
				</div>
				<div class="col-md-8">
					<input type="text" id="site_name" name="config[<?php echo $config->name ?>]" value="<?php echo $config->value ?>" class="form-control">
				</div>
				<div class="clearfix"></div>
			<?php endforeach ?>
		</div>
		<div class="panel-footer text-right tm-padding">
			<button type="submit" class="btn tm-btn">Save</button>
		</div>
	</div>
</form>
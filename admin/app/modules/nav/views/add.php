<?php if(validation_errors()) : ?>
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Error</strong>
    <hr>
    <?php echo validation_errors(); ?>
</div>
<?php endif ?>
<form action="<?php echo current_url() ?>" method="POST" class="form-horizontal" role="form">
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Please complete the form below.</h3>
    </div>
    <div class="box-body tm-padding">            
            <div class="form-group">
                <label for="" class="col-sm-4">Groups</label>
                <div class="col-sm-8">
                <?php 
                echo form_dropdown('groups', $dropdown_groups, 0, array('class'=>'form-control'));    
                ?>
                </div>
            </div>                              
            <div class="form-group">
                <label for="" class="col-sm-4">Parents</label>
                <div class="col-sm-8">
                <?php 
                echo form_dropdown('parent', $dropdown_opt, 0, array('class'=>'form-control'));    
                ?>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-4">Menu's Name</label>
                <div class="col-sm-8">
                <input type="text" name="name" value="<?php echo set_value('name') ?>" class="form-control" id="" placeholder="">
                </div>
            </div>                              
            <div class="form-group">
                <label for="" class="col-sm-4">Link To</label>
                <div class="col-sm-8">
                    <div role="tabpanel">
                         <!-- Nav tabs -->
                         <ul class="nav nav-tabs" role="tablist" id="linkto">
                             <li role="presentation" class="active">
                                 <a href="#home" aria-controls="home" role="tab" data-toggle="tab">Page</a>
                             </li>
                             <li role="presentation">
                                 <a href="#category" aria-controls="tab" role="tab" data-toggle="tab">Category</a>
                             </li>
                             <li role="presentation">
                                 <a href="#module" aria-controls="tab" role="tab" data-toggle="tab">Module</a>
                             </li>
                             <li role="presentation">
                                 <a href="#web" aria-controls="tab" role="tab" data-toggle="tab">Website</a>
                             </li>
                         </ul>
                        <input type="hidden" name="type" id="type" value="#home">
                         <!-- Tab panes -->
                         <div class="tab-content">
                             <div role="tabpanel" class="tab-pane fade in active" id="home">
                                <?php if(isset($pages)) : ?>
                                <?php echo form_dropdown('page', $pages, set_value('page'), 'class="form-control"') ?>
                                <?php else :?>
                                <p style="padding-top: 10px;">No pages found. You need to <a href="<?php echo site_url('pages/add') ?>">add pages</a> here</p>
                                <?php endif ?>
                             </div>
                             <div role="tabpanel" class="tab-pane fade in" id="category">
                                <?php if(isset($category)) : ?>
                                <?php echo form_dropdown('category', $category, set_value('category'), 'class="form-control"') ?>
                                <?php else :?>
                                <p style="padding-top: 10px;">No categories found. You need to <a href="<?php echo site_url('category/add') ?>">add category</a> here</p>
                                <?php endif ?>
                             </div>
                             <div role="tabpanel" class="tab-pane fade in" id="web">
                                <?php echo form_input('web', set_value('web','http://'), 'class="form-control" placeholder="http://"') ?>
                             </div>
                             <div role="tabpanel" class="tab-pane fade in" id="module">
                                <?php echo form_input('module', set_value('module'), 'class="form-control" placeholder="Example: /gallery/list"') ?>
                             </div>
                         </div>
                     </div> 
                </div>
            </div>                              
    </div>
    <div class="panel-footer text-right">
    <a href="<?php echo site_url('nav') ?>" class="btn tm-btn">Cancel</a>
    <button type="submit" class="btn tm-btn">Submit</button>
    </div>
</div>
</form>

<script>
    $('#linkto a').click(function(){
        $('#type').val(($(this).attr('href')));
    })
</script>
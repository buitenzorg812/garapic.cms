<?php if(validation_errors()) : ?>
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Error</strong>
        <hr>
        <?php echo validation_errors(); ?>
    </div>
<?php endif ?>
<form action="<?php echo current_url() ?>" method="POST" class="form-horizontal" role="form">
    <div class="row">

        <div class="col-lg-4">
            <div class="box box-default" >
                <div class="box-header with-border">
                    <h3 class="box-title">Groups</h3>
                </div>
                <div class="list-group" style="padding: 10px;">            
                    <a href="<?php echo site_url('nav') ?>" class="list-group-item tm-noborder <?php echo ($this->uri->total_segments() > 1 ) ? '' : 'active' ?>">All</a>
                    <?php foreach($groups as $group): ?>
                    <a href="<?php echo site_url('nav/filter') .'/'. $group->id  ?>" class="list-group-item tm-noborder <?php echo ($this->uri->segment($this->uri->total_segments()) == $group->slug) ? 'active' :'' ?>"><?php echo $group->name ?></a>
                    <?php endforeach ?>
                </div>
            </div>
            <a href="<?php echo site_url('nav/add') ?>" class="btn tm-btn btn-block">Create New Group</a>
        </div>

        <div class="col-md-8">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Menu</h3>
                </div>
                    <?php if(count($list) > 0) : ?>
                    <?php echo $list ?>
                    <?php else :?>
                    <div class="text-center">
                        No Media Available. <a href="<?php echo base_url('files/add') ?>">Upload New Media</a>
                    </div>
                    <?php endif ?>
            </div>
            <?php echo $pagination ?>
        </div>

    </div>
</form>
<style>
.table>tbody>tr>td,
.table>tbody>tr>th,
.table>tfoot>tr>td,
.table>tfoot>tr>th,
.table>thead>tr>td,
.table>thead>tr>th {
    padding-left: 25px !important;
}    

.table>tbody>tr>td:last-child{
    text-align: right !important;
}    

</style>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setup extends MX_Controller {

    private $table_name     = 'nav';
    private $url            = 'nav/setup';
    private $perpage        = 5;
    private $data           = array();
    private $update         = '';
    private $delete         = '';

    public function __construct()
    {
        parent::__construct();

        // for security check
        $this->menu->has_access();

        // for header
        $this->data          = $this->header->button_header();
        $this->data['title'] = 'Navigations';

        // for button link
        $this->data['button_link'] = (($this->uri->segment(2) == 'add') || ($this->uri->segment(2) == 'update')) ? site_url($this->url) : site_url($this->url).'/add' ;

        // for debug purpose only
        $this->output->enable_profiler(false);
    }

    public function index( $offset = 0 )
    {
        // pagination 
        $this->db
        ->select('id')
        ->from($this->table_name);
        $query = $this->db->get();
        $count                      = $query->num_rows();
        $config['per_page']         = $this->perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url($this->url), $count, $this->perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        // get all data
        // $this->db
        // ->select('id, parent_id, name, DATE_FORMAT(created_at,\'%d %M %Y %T\') created_at')
        // ->from($this->table_name)
        // ->order_by('id')
        // ->limit($this->perpage, $offset);
        // $query = $this->db->get();

        $sql = 'SELECT  id, parent_id, name, DATE_FORMAT(created_at,\'%d %M %Y %T\') created_at
                FROM '.$this->table_name.' feat
                ORDER BY
                        CASE 
                            WHEN parent_id = 0 THEN name
                            ELSE    (
                                    SELECT  name
                                    FROM    '.$this->table_name.' parent 
                                    WHERE   parent.id = feat.parent_id
                                    ) 
                            END,       
                        CASE WHEN parent_id = 0 THEN 1 END DESC,       
                        name';
        $query = $this->db->query($sql);

        // if not empty
        if($query->num_rows() > 0) {
            foreach($query->result() as $list) {
                // check for update access
                if($this->menu->crud_access('update')) {
                    $this->update = '<a href="'.site_url($this->url.'/'.'update'.'/'.$list->id).'" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>';
                }

                // check for delete access
                if($this->menu->crud_access('delete')) {
                    $this->delete = '<a href="'.site_url($this->url.'/'.'delete'.'/'.$list->id).'" class="delete btn btn-xs btn-default"><i class="fa fa-trash"></i></a>';
                }

                // return to table data
                $table_row = array(
                                    $list->name, 
                                    $this->getParent($list->parent_id),
                                    $list->created_at
                );

                // check for additional link access
                if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                    $table_row = array_merge($table_row, array($this->delete.' '.$this->update));
                }

                $this->table->add_row($table_row);
            }

            // generate table header
            $this->table->set_template(array('table_open' => '<table class="table">'));
            if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                $this->table->set_heading('Menu', 'Parent', 'Created At', '');
            } else {
                $this->table->set_heading('Menu', 'Parent', 'Created At');
            }

            // output
            $this->data['list']         = $this->table->generate(); 
            $this->data['content']      = $this->load->view('admin/list', $this->data, TRUE);
        } else {
            // if no data
            $this->data['content']      = $this->load->view('admin/blank', $this->data, TRUE);                    
        }

        // make an output
        $this->load->view('admin/content', $this->data);
    }

    // Add a new item
    public function add()
    {
        // check for write access
        $this->menu->check_access('write');

        $this->db
        ->select('id, parent_id, name')
        ->from($this->table_name)
        ->where('parent_id', '0')
        ->order_by('id');
        $parents = $this->db->get();
        $set_parent[] = 'As Parents';
        if($parents->row()) {        
            foreach($parents->result() as $parent) {
                $set_parent[$parent->id] = $parent->name;
            } 
        }

        $this->data['dropdown_opt']     = $set_parent;

        // get nav groups
        $set_group = array();
        $query = $this->db->get('nav_groups');
        if($query->num_rows() > 0) {        
            foreach($query->result() as $group) {
                $set_group[$group->id] = $group->name;
            } 
        }

        $this->data['dropdown_groups']  = $set_group;


        // get page
        $this->db
        ->select('a.id, b.title')
        ->from('page a')
        ->join('page_lang b', 'b.id = a.id', 'left')        
        ->where('a.type', 'page')
        ->where('b.lang', $this->session->userdata('lang'));
        $query = $this->db->get();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $page) {
                $this->data['pages'][$page->id] = $page->title;
            }
        }

        // get category
        $this->db
        ->select('a.id, b.name')
        ->from('category a')
        ->join('category_lang b', 'b.id = a.id', 'left')        
        ->where('a.type', 'post')
        ->where('b.lang', $this->session->userdata('lang'));
        $query = $this->db->get();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $category) {
                $this->data['category'][$category->id] = $category->name;
            }
        }

        // validation rules
        $this->form_validation->set_rules('name', 'Name', 'required');        

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('nav/add', $this->data, true);
        } else {
            // if not break the rules 
            $type = $this->input->post('type');
            switch($type) {
                case '#home' :
                $slug = $this->input->post('page');
                break;

                case '#category' :
                $slug = $this->input->post('category');
                break;

                case '#web' :
                $slug = $this->input->post('web');
                break;

                case '#module' :
                $slug = $this->input->post('module');
                break;

            }

            $record = array(
                'name'      => $this->input->post('name'),
                'parent_id' => $this->input->post('parent') ,
                'nav_id'    => $this->input->post('groups') ,
                'slug'     => $slug,
                'type'      => substr($this->input->post('type'), 1, strlen($this->input->post('type')))
            );

            // start transaction
            $this->db->trans_start();
            $this->db->insert($this->table_name, $record);
            $this->db->trans_complete();

            // check for status
            if ($this->db->trans_status() === FALSE) {
                $db_error                   = $this->db->error();
                $this->data['message']      = $db_error['message'];
                $this->data['content']      = $this->load->view('nav/add', $this->data, true);
            } else {
                $this->data['message']      = 'Your record has been saved.';
                $this->data['button_link']  = site_url('nav');
                $this->data['content']      = $this->load->view('admin/redirect', $this->data, TRUE);
            }
        }

        // make an output
        $this->load->view('admin/content', $this->data);        
    }

    //Update one item
    public function update( $id = NULL )
    {
        // security check
        $this->menu->check_access('update');

        // get parent
        // we only show the parent list
        $this->db
        ->select('id, category, parent_id')
        ->from($this->table_name)
        ->where('parent_id', '0')
        ->order_by('category');
        $parents = $this->db->get();
        $set_parent[] = 'As Parents';
        if($parents->row()) {        
            foreach($parents->result() as $parent) {
                $set_parent[$parent->id] = $parent->category;
            } 
        }

        $this->data['dropdown_opt'] = $set_parent;

        // get current data
        $query = $this->db->get_where($this->table_name, array('id'=> $id));

        // if no data
        if($query->num_rows() <= 0 ) {
            return show_error('Your data may has been removed or changes by someone.', '' , 'Data Not Found');
            exit;
        }

        // return data
        $this->data['form']     = $query->row();

        // validation rules
        $this->form_validation->set_rules('category', 'Category', 'required');        

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('nav/edit', $this->data, true);
        } else {
            // if not break the rules 
            $record = array(
                'category'      => $this->input->post('category'),
                'parent_id'     => $this->input->post('parent'),
                'description'   => $this->input->post('desc')
            );

            // start transaction
            $this->db->trans_start();
            $this->db->where('id', $this->input->post('id'));
            $this->db->update($this->table_name, $record);
            $this->db->trans_complete();

            // check for status
            if ($this->db->trans_status() === FALSE) {
                $db_error               = $this->db->error();
                $this->data['message']  = $db_error['message'];
                $this->data['content']  = $this->load->view('nav/edit', $this->data, true);
            } else {
                $direct                     = site_url($this->url);
                $this->data['button_link']  = $direct;
                $this->data['message']      = 'Your record has been updated.';
                $this->data['content']      = $this->load->view('admin/redirect', $this->data, TRUE);
            }
        }

        // make an output
        $this->load->view('admin/content', $this->data);        

    }

    //Delete one item
    public function delete( $id = NULL )
    {
        // security check
        $this->menu->check_access('delete');

        $this->db->trans_start();
        $this->db->where('id', $id);
        $this->db->delete($this->table_name);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $message = 'Fail while removed your record.';
        } else {
            $message = 'Your record has been removed.';
        }
        $this->data['message']          = $message;
        $this->data['content']          = $this->load->view('admin/redirect', $this->data, TRUE);
        $this->load->view('admin/content', $this->data);        
    }

    public function getParent($parent_id)
    {   
        $output = '-';
        if(isset($parent_id) && ($parent_id != 0)) {
            $this->db
            ->select('name')
            ->from($this->table_name)
            ->where('id', $parent_id);
            $parents = $this->db->get();
            if($parents->num_rows() > 0 ){
                $parent = $parents->row();                
                $output = $parent->name;            
            }
        }
        return $output;
    }

}

/* End of file Category.php */
/* Location: ./application/modules/admin/controllers/Category.php */

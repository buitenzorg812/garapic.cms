<?php if(isset($message)) : ?>
<div class="alert alert-danger">
    <?php echo $message; ?>
</div>
<?php endif ?>
<?php if(validation_errors()) : ?>
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Error</strong>
    <hr>
    <?php echo validation_errors(); ?>
</div>
<?php endif ?>

<?php echo form_open(current_url(), 'class="form-horizontal" role="form"');?>
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Please complete the form below.</h3>
    </div>
    <div class="box-body tm-padding">            
        <div class="form-group">
            <label for="" class="col-sm-4">Group Name</label>
            <div class="col-sm-8">
            <?php echo form_input('name',set_value('name'),'class="form-control"');?>
            </div>
        </div>
             
        <div class="form-group">
            <label for="" class="col-sm-4">Description</label>
            <div class="col-sm-8">
            <?php echo form_input('desc',set_value('desc'),'class="form-control"');?>
            </div>
        </div>                                 
</div>
<div class="panel-footer text-right">
    <a href="<?php echo site_url('groups') ?>" class="btn tm-btn">Cancel</a>
    <button type="submit" class="btn tm-btn">Submit</button>
</div>
</div>

<?php echo form_close();?>

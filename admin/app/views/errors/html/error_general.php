<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?php echo $heading; ?></title>
<style type="text/css">

::selection { background-color: #E13300; color: white; }
::-moz-selection { background-color: #E13300; color: white; }

body {
	overflow: hidden;
	background-color: #fff;
	margin: 40px;
	font: 12pt/0.5 normal Helvetica, Arial, sans-serif;
	color: #4F5155;
	text-transform: uppercase;
}

a {
	color: #003399;
	background-color: transparent;
	font-weight: normal;
}

h1 {
	color: #444;
	font-family: Arial, serif;
	font-weight: bold;
	font-size: 32pt;
	background-color: transparent;
	letter-spacing: 4px;
	padding: 50px 0 0 0 ;
}

code {
	font-family: Consolas, Monaco, Courier New, Courier, monospace;
	font-size: 12px;
	background-color: #f9f9f9;
	border: 1px solid #D0D0D0;
	color: #002166;
	display: block;
	margin: 14px 0 14px 0;
	padding: 12px 10px 12px 10px;
}

#container {
	margin: 10px;
	text-align: center;
	position: relative;
}

p {
	margin: 0 auto;
	letter-spacing: 5px;
	font-size: 10px;
	width: 50%;
	text-align: center;	
	line-height: 2;
}

em {
	font-weight: bolder;
	letter-spacing: 2px;
	font-size: 10px;
}

br {
	line-height: 30px;
}

.btn {
	background-color: #333;
	color: #fff;
	padding:15px 20px;
	border-radius: 4px;
	border: none;
	text-decoration: none;
	font-size: 10px;
	letter-spacing: 4px;
}
.copy {
	left: 100px;
	top:335px;
	width: 100%;
	text-align: center;
	position: absolute;
	font-style: italic;
	letter-spacing: 4px;
	font-size: 7px;
}

</style>
</head>
<body>
	<div id="container">
		<h1><?php echo $heading; ?></h1>
		<img src="<?php echo base_url().IMG.'404.gif' ?>" width="420" alt="">
		<div class="copy">&copy; Markus Magnusson</div>
		<p><?php echo $message; ?>
		<br>
		<br>
		<?php if(trim(strtoupper($heading)) == 'ACCESS DENIED') : ?>
		<a href="<?php echo site_url() ?>" class="btn">Login</a>
		<?php else :?>
		<a href="#" class="btn" onclick="history.back()">Back</a>
		<?php endif ?>
	</div>
</body>
</html>
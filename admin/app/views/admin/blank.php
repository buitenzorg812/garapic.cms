<div class="tm-padding text-center" style="padding-top: 100px">
    <div class="tm-icon">
        <i class="fa fa-3x fa-warning"></i>
    </div>
    <h3 style="line-height: 1.6em">
        <?php if(isset($message)) : ?>
        <?php echo $message ?>
        <?php else :?>
        Opps ... Data is not available yet.
        <?php endif ?>
    </h3>
    <?php if(isset($button_link) && isset($button_text)) : ?>
    <p>Click the button below to start recording your data.</p>
    <?php endif ?>
    <br>
    <br>
    <br>
    <?php if(isset($button_link) && isset($button_text)) : ?>
    <a href="<?php echo $button_link ?>" class="btn tm-btn"><?php echo $button_text ?></a>
    <?php endif ?>
</div>

<?php $this->load->view("admin/header") ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">

          <!-- Your Page Content Here -->
          <?php echo $content ?>

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php $this->load->view("admin/footer") ?>

<div class="tm-padding text-center">
    <div class="tm-icon">
        <i class="fa fa-3x fa-check-circle-o"></i>
    </div>
    <h3><?php echo $message ?></h3>
    <p>Please wait for <span id="countdown">2</span> seconds.</p>
    <br>    
    <br>
    <a href="<?php echo $button_link ?>" id="goback">Direct Manually</a>
</div>
<script type="text/javascript">
// Redirect Timer
function doUpdate() {
    $('#countdown').each(function() {
        var count = parseInt($(this).html());
        if (count !== 0) {
            $(this).html(count - 1);
        } else {
            window.location = $('#goback').attr('href');
        }
    });
};
setInterval(doUpdate, 1000);
</script>
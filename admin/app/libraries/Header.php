<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Header 
{
    private $ci;
    private $config = array();

    public function __construct()
    {
        $this->ci =& get_instance();
        $this->ci->load->library('uri');
    }

    public function button_header() 
    {
        $last                     = $this->ci->uri->total_segments();
        $record_num               = $this->ci->uri->segment($last);
        $progress_num               = $this->ci->uri->segment($last-1);
        if($record_num == 'add' || $progress_num == 'delete' || $progress_num == 'edit' ) {
            $data['button_text'] = 'List';
            $data['button_link'] = site_url('admin/' . $this->ci->uri->segment(2));
        } else {
            $data['button_text'] = 'Add';
            $data['button_link'] = site_url('admin/'.$this->ci->uri->segment(2).'/add');
        }
        return $data;  
    }

    public function account_name($user_id) 
    {
        $table = 'cms_users';
        $this->ci->db
        ->where('id', $user_id)
        ->limit(1);
        $account = $this->ci->db->get($table);
        if($account->num_rows() > 0 ) {
            $profile = $account->row();
            return $profile->first_name . ' ' . $profile->last_name;
        } else {
            return 'Stranger In Your Heart';        
        }
    }

    public function load_config() 
    {
        $all_config     = $this->ci->db->get('cms_config');
        foreach ($all_config->result() as $cfg) {
            $config[$cfg->name] = json_decode($cfg->value);
        }
        return $config;
    }

    public function is_offline() 
    {
        $this->config                = $this->load_config();
        if (isset($this->config['offline']) && $this->config['offline'] == true) {
            redirect(site_url('/maintenance'));
        }        
    }

}

/* End of file Header.php */
/* Location: ./application/libraries/Header.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Menu {

	var $ci;

	function __construct() {
		$this->ci = &get_instance();
	}

    public function access($user_group)
    {
		// Modul List
		$menu 				= array();
        $this->ci->db        
        ->from('cms_modules a')
        ->join('cms_priviledges b', 'b.modules_id = a.id', 'left') 
        ->where('b.groups_id', $user_group)
        ->where('a.parent', 0)
        ->where('a.show', 1)
        ->order_by('a.orders');
		$query = $this->ci->db->get();

		if($query->num_rows() > 0) {
			foreach ($query->result() as $modul) {
				$menu[$modul->id]['menu']	= $modul->name;
				$menu[$modul->id]['path']	= $modul->path;
                $menu[$modul->id]['icon']   = $modul->icon;
				$menu[$modul->id]['orders']	= $modul->orders;
                $this->ci->db
                ->from('cms_modules a')
                ->join('cms_priviledges b', 'b.modules_id = a.id', 'left')
                ->where('b.groups_id', $user_group)
                ->where('a.parent', $modul->id)
                ->where('a.show', 1)
                ->order_by('a.orders');
				$sub_modul 	= $this->ci->db->get();

				if($sub_modul->num_rows() > 0 ){
					foreach ($sub_modul->result() as $sub) {
						$menu[$modul->id]['sub'][$sub->path] = $sub->name;
					}
				} else {
					$menu[$modul->id]['sub'] = array();
				}
			}
		}
		return $menu;
    }

    public function has_access()
    {
        // check current modules
        $modules = $this->ci->uri->segment(1);

        // check into database
        $this->ci->db
        ->select('cms_groups.name')
        ->from('cms_groups')
        ->join('cms_priviledges','cms_priviledges.groups_id = cms_groups.id')
        ->join('cms_modules','cms_modules.id = cms_priviledges.modules_id')
        ->where('cms_groups.id', $this->ci->session->userdata('group_id'))
        ->where('cms_modules.path', $modules)
        ->limit(1);
        $query = $this->ci->db->get();
        if($query->num_rows() > 0 ) {
            return true;
        } else {
            return show_error('You have no access to view this page.','404','Access Denied');
            exit;
        }

    }

    public function crud_access($access)
    {
        switch ($access) {
            case 'write'    : $field_check = 'can_write'; break;
            case 'update'   : $field_check = 'can_update'; break;
            case 'delete'   : $field_check = 'can_delete'; break;            
        }

        $this->ci->db
        ->select($field_check)
        ->from('cms_priviledges')
        ->where($field_check, 1)
        ->where('groups_id', $this->ci->session->userdata('group_id'))
        ->limit(1);

        $query = $this->ci->db->get();
        $check = $query->row();
        if($query->num_rows() > 0 ) {
            return true;
        } else {
            return false;
        }
    }

    public function check_access($access)
    {
        if(!($this->crud_access($access))) {
            return show_error('You have no access to '.$access.' on this page.');
            exit;
        }
        return true;

    }

}

/* End of file Login.php */
<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<title><?php echo get_conf('site_name') ?></title>
<meta charset="utf-8" />    
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content="Eddy Subratha" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta name="description" content="<?php echo get_conf('meta_description') ?>"> 
<meta name="robots" content="<?php echo get_conf('meta_robots') ?>">
<link rel="icon" type="image/x-icon" href="<?php echo base_url('favicon.ico') ?>" />
<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url("/assets/css/bootstrap.css") ?>" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url("/assets/css/style.css") ?>" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url("/assets/css/font-icons.css") ?>" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url("/assets/css/animate.css") ?>" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url("/assets/css/magnific-popup.css") ?>" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url("/assets/css/responsive.css") ?>" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url("/assets/css/dark.css") ?>" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url("/assets/css/custom.css") ?>" type="text/css" />
<!--[if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
<script type="text/javascript" src="<?php echo base_url("/assets/js/jquery.js") ?>"></script>
<script type="text/javascript" src="<?php echo base_url("/assets/js/plugins.js") ?>"></script>
<script type="text/javascript" src="//maps.google.com/maps/api/js?sensor=true"></script>
<script type="text/javascript" src="<?php echo base_url("/assets/js/jquery.gmap.js") ?>"></script>
</head>
<body>

    <div id="wrapper" class="clearfix">
        <?php $this->load->view('master/nav') ?>

        <?php if($this->uri->segment(1) != '') : ?>
        <section id="page-title" class="nomargin page-title-dark" style="background: transparent url(<?php echo base_url() . UPLOADS ?>/slider.jpg) center center no-repeat; background-size: cover;">
            <div class="container clearfix">
                <h1><?php echo $title ?></h1>
            </div>
        </section>
        <?php endif ?>
        
        <?php if(current_url() == base_url()) $this->load->view('slider') ?>

        <section id="content">
            <div class="content-wrap">
            <?php echo $content ?>                
            </div>
        </section>


        <footer id="footer" class="dark noborder" style="background: url('images/footer-bg.jpg') center top repeat; background-size: 100% 100%">
            <div class="container">
                <div class="footer-widgets-wrap notoppadding clearfix">
                    <div class="col-md-4 ">
                        <div class="heading-block topmargin noborder">
                            <h4 class="text-warning">Informasi</h4> 
                        </div>
                        <div class="widget clearfix">                               
                            <div style="background: url('<?php echo base_url('/assets')?>/images/world-map.png') no-repeat bottom left; ">
                                <address>
                                    <strong>SEKRETARIAT<br>IKATAN GEOGRAFI INDONESIA</strong>
                                    <br>
                                    <?php echo get_conf('Alamat 1')?><br>
                                    <?php echo get_conf('Alamat 2')?> 
                                </address>
                                <!--
                                <abbr title="Kontak 1"><strong>CP:</strong></abbr> <?php echo get_conf('CP1') ?><br>
                                <abbr title="Kontak 2"><strong>CP:</strong></abbr> <?php echo get_conf('CP2') ?><br>
                                -->
                                <abbr title="Kontak 1"><strong>Phone:</strong></abbr> <?php echo get_conf('Telepon') ?><br>
                                <abbr title="Kontak 1"><strong>Fax:</strong></abbr> <?php echo get_conf('Fax') ?><br>
                                <abbr title="Email Address"><strong>Email:</strong></abbr> <?php echo get_conf('Email') ?>
                            </div>

                        </div>  
                        <br>
                        <br>
                        <br>
                        <div class="widget clearfix">

                            <div class="row">

                                <div class="col-md-6 clearfix bottommargin-sm">
                                    <a href="#" class="social-icon si-dark si-colored si-facebook nobottommargin" style="margin-right: 10px;">
                                        <i class="icon-facebook"></i>
                                        <i class="icon-facebook"></i>
                                    </a>
                                    <a href="#"><small style="display: block; margin-top: 3px;"><strong>Like us</strong><br>on Facebook</small></a>
                                </div>
                                <div class="col-md-6 clearfix">
                                    <a href="#" class="social-icon si-dark si-colored si-rss nobottommargin" style="margin-right: 10px;">
                                        <i class="icon-rss"></i>
                                        <i class="icon-rss"></i>
                                    </a>
                                    <a href="#"><small style="display: block; margin-top: 3px;"><strong>Subscribe</strong><br>to RSS Feeds</small></a>
                                </div>

                            </div>

                        </div>

                    </div>
                
                    <div class="col-md-4">                        
                        <div class="heading-block topmargin noborder">
                            <h4 class="text-warning">SITUS TERKAIT</h4>
                        </div>
                        <div class="widget widget_links clearfix">
                            <ul>
                                <li><a href="//www.big.go.id">Badan Informasi Geospasial</a></li>
                                <li><a href="//www.unj.ac.id">Universitas Negeri Jakarta</a></li>
                                <li><a href="//www.ugm.ac.id">Universitas Gadjah Mada Yogyakarta</a></li>
                                <li><a href="#">Ikatan Geograf Indonesia</a></li>
                            </ul>

                        </div>
                        <div class="heading-block topmargin noborder">
                            <h4 class="text-warning">PENGUMUMAN</h4>
                        </div>
                        <div class="widget widget_links clearfix">
                            <ul>
                                <li><a href="<?php echo base_url() ?>uploads/Leaflet depan copy.jpg">Leaflet (Depan)</a></li>
                                <li><a href="<?php echo base_url() ?>uploads/Leaflet Belakangcopy.jpg">Leaflet (Belakang)</a></li>
                                <li><a href="<?php echo base_url() ?>uploads/Template_Abstrak_PITIGIXVIII2015_NamaPenulis.docx">Template Abstrak PIT IGI XVIII 2015 (Depan)</a></li>
                                <li><a href="<?php echo base_url() ?>uploads/Poster Seminar_PITIGI.jpg">Poster Seminar</a></li>
                            </ul>

                        </div>
                    </div>

                    <div class="col-md-4 ">
                        <div class="heading-block topmargin noborder">
                            <h4 class="text-warning">Galeri</h4>
                        </div>
                        <div class="widget widget_gallery clearfix">
                            <div class="masonry-thumbs col-3" data-lightbox="gallery">
                                <?php foreach($footer_gallery as $pic ) : ?>
                                <a href="<?php echo site_url('media/') .'/'. $this->media->images($pic->id) ?>" data-lightbox="gallery-item"><img class="image_fade" style="height:90px !important; overflow:hidden;" src="<?php echo site_url('media/') .'/'. $this->media->images($pic->id) ?>" alt="<?php echo $pic->alt ?>"></a>
                                <?php endforeach ?>
                            </div>

                        </div>    
                        <br><br>
                        <div class="heading-block nomargin topmargin noborder">
                            <h4 class="text-warning">Statistik</h4>
                        </div>
                        <br>

                        <div class="counter counter-small core-counter">
                            <span data-from="1" data-to="<?php echo get_user_counter() ?>" data-refresh-interval="5" data-speed="300" data-comma="true"></span>
                            <h5>Kunjungan</h5>
                            <span data-from="1" data-to="<?php echo get_user_online() ?>" data-refresh-interval="1" data-speed="300" data-comma="true"></span>
                            <h5 class="nobottommargin">Online</h5>
                        </div>

                    </div>

                </div><!-- .footer-widgets-wrap end -->

            </div>

            <!-- Copyrights
            ============================================= -->
            <div id="copyrights">

                <div class="container clearfix">

                    <div class="col_half">
                        &copy; Ikatan Geografi Indonesia 2015 - <?php echo date('Y') ?> <br>Silahkan menyebarluaskan informasi yang anda temukan.
                    </div>

                    <div class="col_half col_last tright">
                        <div class="fright clearfix">
                            Dikelola Oleh<br>Ikatan Geografi Indonesia
                        </div>
                    </div>

                </div>

            </div><!-- #copyrights end -->

        </footer><!-- #footer end -->
    </div> 

    <div id="gotoTop" class="icon-angle-up"></div>
    <script type="text/javascript" src="<?php echo base_url('assets/js/functions.js') ?>"></script>
</body>
</html>
<div class="container clearfix center topmargin bottommargin"> 
    <div class="heading-block center">
        <h1><?php echo $title ?></h1>
        <?php echo $message ?>        
    </div>    
    <a href="<?php echo base_url() ?>" class="button button-3d button-rounded button-xlarge button-black"><i class="icon-arrow-left"></i> Beranda</a>
</div>

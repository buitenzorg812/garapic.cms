<div id="top-bar">
    <div class="container clearfix">
        <div class="col_half nobottommargin">
            <div class="top-links">
                <ul>
                    <li><a href="<?php echo site_url() ?>">Beranda</a></li>
                    <li><a href="<?php echo site_url('sitemap') ?>">Peta Situs</a></li>
                </ul>
            </div>
        </div>
        <div class="col_half col_last fright nobottommargin">
            <div class="top-links">
                <ul>
                    <?php if(isset($this->session->userdata['email'])) : ?>
                    <li><a href="<?php echo site_url('member/profile') ?>">Profil</a></li>
                    <li><a href="<?php echo site_url('member/logout') ?>">Logout</a></li>
                    <?php else : ?>
                    <li><a href="<?php echo site_url('member/register') ?>">Pendaftaran</a></li>
                    <li><a href="<?php echo site_url('member/login') ?>">Login Anggota</a></li>
                    <?php endif ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<header id="header" class="sticky-style-2">
    <div class="container clearfix">
        <div id="logo">
            <a href="<?php echo site_url() ?>" class="standard-logo" data-dark-logo="<?php echo base_url('/assets')?>/img/logo-dark.png"><img src="<?php echo base_url('/assets')?>/img/logo.png" alt="<?php echo get_conf('site_name') ?>"></a>
            <a href="<?php echo site_url() ?>" class="retina-logo" data-dark-logo="<?php echo base_url('/assets')?>/img/logo-dark@2x.png"><img src="<?php echo base_url('/assets')?>/img/logo@2x.png" alt="<?php echo get_conf('site_name') ?>"></a>
        </div>
        <ul class="header-extras">
            <li>
                <i class="i-plain icon-call nomargin"></i>
                <div class="he-text">
                    Hubungi
                    <span><?php echo get_conf('Telepon') ?></span>
                </div>
            </li>
            <li>
                <i class="i-plain icon-email3 nomargin"></i>
                <div class="he-text">
                    Sapa Kami
                    <span><?php echo get_conf('Email') ?></span>
                </div>
            </li>
        </ul>
    </div>
    <div id="header-wrap">
        <nav id="primary-menu" class="style-2 center">
            <div class="container clearfix">
                <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
                <ul>
                    <li><a href="<?php echo site_url() ?>"><div>Beranda</div></a></li>
                    <li><a href="#"><div>Tentang Organisasi</div></a>
                        <ul>
                            <li><a href="<?php echo site_url('read/organisasi') ?>"><div>Sejarah IGI</div></a>
                            <li><a href="<?php echo site_url('read/bentuk-organisasi') ?>"><div>Bentuk Organisasi</div></a></li>
                        </ul>
                    </li>
                    <li><a href="<?php echo site_url('archives') ?>"><div>Berita</div></a></li>
                    <li><a href="<?php echo site_url('gallery') ?>"><div>Galeri</div></a></li>
                    <li><a href="<?php echo site_url('announce') ?>"><div>Pengumuman</div></a></li>
                    <li><a href="<?php echo site_url('schedule') ?>"><div>Agenda Kegiatan</div></a></li>
                    <li><a href="<?php echo site_url('member') ?>"><div>Anggota</div></a></li>
                </ul>
            </div>
        </nav>
    </div>
</header>

<script>
    $('.notready').click(function(e){
        e.preventDefault();
        alert('Belum Tersedia');
        return false;
    })
</script>
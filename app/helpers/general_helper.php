<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('get_conf'))
{
	function get_conf($name = '')
	{
		$CI 		=& get_instance();
		$sql 		= "SELECT value FROM cms_config WHERE name='$name'";
		$query 		= $CI->db->query($sql);	 
		if ($query->num_rows() > 0) {
			$setting	= $query->row();
			return $setting->value;
		}
	}
}

if ( ! function_exists('get_remote_url'))
{
	function get_remote_url()
	{
		$remote_url 		= get_conf('site_url');
		return $remote_url;
	}
}

if ( ! function_exists('get_remote_path'))
{
	function get_remote_path()
	{
		$resource_path 		= MEDIA;
		return get_remote_url().$resource_path;
	}
}

if ( ! function_exists('get_user_counter'))
{
	function get_user_counter()
	{
		// Define CI
		$CI 		=& get_instance();
		$count 		= $CI->db->count_all('cms_stats');

		if($count > 0) {
			return $count; 
		} else { 
			return 0; 
		} 
	}
}

if ( ! function_exists('get_user_online'))
{
	function get_user_online()
	{
		// Define CI
		$CI 		=& get_instance();

		/* Define how long the maximum amount of time the session can be inactive. */ 
		define("MAX_IDLE_TIME", 3); 

		if ( $directory_handle = opendir( session_save_path() ) ) { 
			$count = 0; 
			while ( false !== ( $file = readdir( $directory_handle ) ) ) { 
				if($file != '.' && $file != '..'){ 
					// Comment the 'if(...){' and '}' lines if you get a significant amount of traffic 
					if(time()- fileatime(session_save_path() . '/' . $file) < MAX_IDLE_TIME * 60) { 
						$count++; 
					} 
				} 
			}
			$sql = "INSERT IGNORE INTO cms_stats(`sessid`) VALUES ('".session_id()."')  ";			
			$CI->db->query($sql);	 
			closedir($directory_handle); 
			return $count; 
		} else { 
			return false; 
		} 
	}
}

function debug($string) 
{
	echo '<pre>';
	print_r($string);
	echo '</pre>';
	exit;
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Publikasi_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getPub($data)
	{
		$this->db->select('*');
		$this->db->from('member');
		$this->db->where('email', $data['email']);
		$this->db->join('member_data', 'member.member_id = member_data.member_id', 'left');
		$this->db->join('member_publikasi', 'member.member_id = member_publikasi.member_id', 'left');
		$query = $this->db->get();
		if ($query->num_rows() >0){
			foreach ($query->result() as $data) {
				$memBio['member_id'] = $data;
			}
			return $memBio;
		}
	}
	
	public function addPub()
	{
		
		$biodata = array(
			'member_id'		=> $this->input->post('member_id'),
			'pub_title'		=> $this->input->post('prof_pub_title'),
			'pub_partner'	=> $this->input->post('prof_pub_part'),
			'pub_type'		=> $this->input->post('prof_pub_type'),
			'pub_isbn'		=> $this->input->post('prof_pub_isbn'),
			'pub_year'		=> $this->input->post('prof_pub_year'),
			'pub_abstract'	=> $this->input->post('prof_pub_abs'),
			'pub_file'		=> $this->input->post('prof_pub_file'),
			'pub_link'		=> $this->input->post('prof_pub_link'),
			'last_update'	=> date('Y-m-d H:i:s')
		);
		
		$this->db->select('*');
		$this->db->from('member_publikasi');
		$query = $this->db->get();
		$result = $query->result_array();
		$count  = count($result);
		
		if (empty($count)) {
			
			$insert = $this->db->insert('member_publikasi', $biodata);
			$this->db->query($insert);
			return $this->db->insert_id();
			
		} else {

			$insert = $this->db->update('member_publikasi', $biodata);
			$this->db->query($insert);
			return $this->db->insert_id();
		}
	}
}
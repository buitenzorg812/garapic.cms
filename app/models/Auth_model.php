<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth_model extends CI_Model
{	

	private $status = array('pending', 'approved');

	public function __construct()
	{
		parent::__construct();
	}
	
	public function insertMember($data)
	{
		// Get data from registration form
		$reg_form = array(
			'realname'	=> $data['reg_name'],
			'email'		=> $data['reg_mail'],
			'status'	=> $this->status[0],
			'reg_date'	=> date('Y-m-d H:i:s')
		);
		
		$insert = $this->db->insert_string('member', $reg_form);
		$this->db->query($insert);
		
		$biodata['member_id'] = $this->db->insert_id();
		return $this->db->insert_id();
	}
	
	public function insertToken($member_id)
	{
		$token = substr(sha1(rand()), 0, 30);
		$date = date('Y-m-d');
		
		$string = array(
			'token'		=> $token,
			'member_id'	=> $member_id,
			'created'	=> $date
		);
		
		$query = $this->db->insert_string('member_tokens', $string);
		$this->db->query($query);
		return $token;
	}
	
	public function isTokenValid($token)
	{
		// check is password already set up
		$q = $this->db->get_where('member_tokens', array('token' => $token));
		if($q->num_rows() > 0){
			$row 		= $q->row();			
			$created	= $row->created;
			$createdTS	= strtotime($created);
			$today		= date('Y-m-d');
			$todayTS	= strtotime($today);
			
			if($createdTS != $todayTS){
				return false;
			}
			
			$user_info = $this->getUserInfo($row->member_id);
			return $user_info;
		} else {
			return false;
		}
	}
	
	public function getUserInfo($id)
	{
		$q = $this->db->get_where('member', array('member_id' => $id), 1);
		if($this->db->affected_rows() > 0){
			$row = $q->row();
			return $row;
		} else {
			error_log('no user found getUserInfo('.$id.')');
			return false;
		}
	}
	
	public function updateUserInfo($post)
	{
		// debug($post);
		// Get data from password form
		$data = array(
			'password'		=> $post['reg_pass'],
			'status'		=> $this->status[1],
			'last_login'	=> date('Y-m-d H:i:s')
		);
		
		$this->db->where('member_id', $post['member_id']);
		$this->db->update('member', $data);
		$success = $this->db->affected_rows();
		
		if(!$success){
			error_log('Unable to updateUserInfo('.$post['member_id'].')');
			return false;
		}
		
		$user_info = $this->getUserInfo($post['member_id']);
		return $user_info;
	}
	
	public function checkLogin($post)
	{
		$this->load->library('password');
		$this->db->select('*');
		$this->db->where('email', $post['reg_mail']);
		$query = $this->db->get('member');
		$userInfo = $query->row();
		
		if(!$this->password->validate_password($post['reg_pass'], $userInfo->password)){
			error_log('Unsuccessful login attempt('.$post['reg_mail'].')');
			return false; 
		}
		
		$this->updateLoginTime($userInfo->member_id);
		
		unset($userInfo->password);
		return $userInfo; 
	}
	
	public function updateLoginTime($id)
	{
		$this->db->where('member_id', $id);
		$this->db->update('member', array('last_login' => date('Y-m-d H:i:s')));
		return;
	}
	
	public function getUserInfoByEmail($email)
	{
		$q = $this->db->get_where('member', array('email' => $email), 1);
		if($this->db->affected_rows() > 0){
			$row = $q->row();
			return $row;
		} else {
			error_log('no user found getUserInfo('.$email.')');
			return false;
		}
	}
	
	public function updatePassword($post)
	{
		$this->db->where('member_id', $post['member_id']);
		$this->db->update('member', array('password' => $post['reg_pass'])); 
		$success = $this->db->affected_rows(); 
		
		if(!$success){
			error_log('Unable to updatePassword('.$post['member_id'].')');
			return false;
		}
		return true;
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jabatan_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getJab($data)
	{
		$this->db->select('*');
		$this->db->from('member');
		$this->db->where('email', $data['email']);
		$this->db->join('member_data', 'member.member_id = member_data.member_id', 'left');
		$this->db->join('member_jabatan', 'member.member_id = member_jabatan.member_id', 'left');
		$query = $this->db->get();
		if ($query->num_rows() >0){
			foreach ($query->result() as $data) {
				$memBio['member_id'] = $data;
			}
			return $memBio;
		}
	}
	
	public function addJab()
	{
		$biodata = array(
			'member_id'		=> $this->input->post('member_id'),
			'jbt_type'		=> $this->input->post('prof_jbt_type'),
			'jbt_office'	=> $this->input->post('prof_jbt_offc'),
			'jbt_periode'	=> $this->input->post('prof_jbt_year'),
			'last_update'	=> date('Y-m-d H:i:s')
		);
		
		$this->db->select('*');
		$this->db->from('member_jabatan');
		$query = $this->db->get();
		$result = $query->result_array();
		$count  = count($result);
		
		if (empty($count)) {
			
			$insert = $this->db->insert('member_jabatan', $biodata);
			$this->db->query($insert);
			return $this->db->insert_id();
			
		} else {

			$insert = $this->db->update('member_jabatan', $biodata);
			$this->db->query($insert);
			return $this->db->insert_id();
		}
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pengabdian_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getAbdi($data)
	{
		$this->db->select('*');
		$this->db->from('member');
		$this->db->where('email', $data['email']);
		$this->db->join('member_data', 'member.member_id = member_data.member_id', 'left');
		$this->db->join('member_abdi', 'member.member_id = member_abdi.member_id', 'left');
		$query = $this->db->get();
		if ($query->num_rows() >0){
			foreach ($query->result() as $data) {
				$memBio['member_id'] = $data;
			}
			return $memBio;
		}
		
	}
	
	public function addAbdi()
	{
		
		$biodata = array(
			'member_id'		=> $this->input->post('member_id'),
			'abdi_title'	=> $this->input->post('prof_abdi_title'),
			'abdi_partner'	=> $this->input->post('prof_abdi_part'),
			'abdi_year'		=> $this->input->post('prof_abdi_year'),
			'abdi_stake'	=> $this->input->post('prof_abdi_stake'),
			'last_update'	=> date('Y-m-d H:i:s')
		);
		
		$this->db->select('*');
		$this->db->from('member_abdi');
		$query = $this->db->get();
		$result = $query->result_array();
		$count  = count($result);
		
		if (empty($count)) {
			
			$insert = $this->db->insert('member_abdi', $biodata);
			$this->db->query($insert);
			return $this->db->insert_id();
			
		} else {

			$insert = $this->db->update('member_abdi', $biodata);
			$this->db->query($insert);
			return $this->db->insert_id();
		}
	}
}
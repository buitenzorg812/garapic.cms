<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pendidikan_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getEdu($data)
	{
		$this->db->select('*');
		$this->db->from('member');
		$this->db->where('email', $data['email']);
		$this->db->join('member_data', 'member.member_id = member_data.member_id', 'left');
		$this->db->join('member_edu', 'member.member_id = member_edu.member_id', 'left');
		$query = $this->db->get();
		if ($query->num_rows() >0){
			foreach ($query->result() as $data) {
				$memBio['member_id'] = $data;
			}
			return $memBio;
		}
	}
	
	public function addEdu()
	{
		$biodata = array(
			'member_id'		=> $this->input->post('member_id'),
			'sarjana'		=> $this->input->post('prof_edu_level'),
			'jurusan'		=> $this->input->post('prof_edu_jur'),
			'kampus'		=> $this->input->post('prof_edu_camp'),
			'lulus'			=> $this->input->post('prof_edu_year'),
			'skripsi'		=> $this->input->post('prof_edu_title'),
			'last_update'	=> date('Y-m-d H:i:s')
		);
		
		$this->db->select('*');
		$this->db->from('member_edu');
		$query = $this->db->get();
		$result = $query->result_array();
		$count  = count($result);
		
		if (empty($count)) {
			
			$insert = $this->db->insert('member_edu', $biodata);
			$this->db->query($insert);
			return $this->db->insert_id();
			
		} else {

			$insert = $this->db->update('member_edu', $biodata);
			$this->db->query($insert);
			return $this->db->insert_id();
		}
	}
}
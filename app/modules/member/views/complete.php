<section class="container clearfix topmargin">
	<div class="col_two_third clearfix">
		<?php if($custom_error == '') : ?>
		<div class="panel panel-default noshadow">
			<div class="panel-heading">
	            <h3 class="nobottommargin" style="text-transform: uppercase;">Verifikasi Akun Anggota</h3>
			</div>
			<div class="panel-body">
				<p style="margin-bottom: 0px;">
					Yang terhormat Bapak/Ibu/Saudara/i <strong><?php echo $realName; ?></strong>.<br />
					Email anda <strong><?php echo $email;?></strong> telah berhasil kami verifikasi.<br />
					Silahkan lengkapi password anda untuk mengakses website ini.
				</p>
			</div>
		</div>
		<?php else: ?>
		<div class="panel panel-danger">
			<div class="panel-heading">
				<h3>Galat</h3>
			</div>
			<div class="panel-body">
				<ul class="iconlist" style="line-height: 2;">
					<?php echo $custom_error; ?>
				</ul>
			</div>
		</div>
		<?php endif; ?>
		
		<!-- FORM EMAIL CONFIRMATION BEGIN -->
		<?php echo form_open(base_url('member/complete/token/'.$token), 'class="form-horizontal nobottommargin" id="complete-form" name="complete-form"') ?>
			<div class="panel panel-default noshadow">
				<div class="panel-body">
					<div class="form-group">
						<label for="textinput" class="control-label col-md-4">Password</label>
						<div class="col-md-8">
							<input name="reg_pass" value="<?php echo set_value('reg_pass') ?>" class="required form-control input-block-level" type="password">
							<p class="help-block">Panjang Password Minimal 6 Karakter</p>
						</div>
					</div>
					<div class="form-group">
						<label for="textinput" class="control-label col-md-4">Konfirmasi Password</label>
						<div class="col-md-8">
							<input name="reg_passed" value="<?php echo set_value('reg_passed') ?>" class="required form-control input-block-level" type="password">
							<p class="help-block">Konfirmasi Password harus sama dengan Password.</p>
						</div>
					</div>
				</div>
				
				<div class="panel-footer">
					<div class="form-group nobottommargin">
						<?php echo form_hidden('member_id', $member_id);?>
						<div class="text-center col-md-12">
							<div id="button1idGroup" class="btn-group" role="group" aria-label="Button Group">
								<button type="submit" class="button button-3d button-black nomargin" id="complete-form-submit" name="submit" value="submit">Submit</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
		<!-- FORM EMAIL CONFIRMATION END -->
	</div>
    <?php $this->load->view('sidebar') ?>
</section>
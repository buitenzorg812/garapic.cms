<?php echo validation_errors() ?>
<?php echo form_open(current_url(), 'class="form-horizontal nobottommargin" id="biodata-form" name="biodata-form"') ?>
<div class="panel panel-default noshadow">
	<div class="panel-heading">
		<h3 class="nobottommargin" style="text-transform: uppercase;">Biodata Anggota</h3>
	</div>
	<div class="panel-body">
		<!-- FORM BIODATA BEGIN -->
			Mohon lengkapi formulir biodata anggota berikut ini.
			<hr />

			<div class="form-group">
				<label class="col-md-4 control-label" for="prof_nama">Nama Lengkap * </label>
				<div class="col-md-8">
					<input name="prof_nama" value="<?php echo set_value('prof_nama', $prof->realname); ?>" class="sm-form-control required" type="text">					
					<?php echo form_hidden('member_id', $prof->member_id);?>
				</div>
			</div>			
			<div class="form-group">
				<label class="col-md-4 control-label" for="prof_birth_city">Tempat Lahir *</label>
				<div class="col-md-8">
					<input name="prof_birth_city" value="<?php echo set_value('prof_birth_city', $prof->birth_city); ?>" class="sm-form-control required" type="text">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-4 control-label" for="prof_birth_date">Tanggal Lahir *</label>
				<div class="col-md-8">
					<input id="birthdate" name="prof_birth_date" value="<?php echo set_value('prof_birth_date', $prof->birth_date); ?>" class="sm-form-control required" type="text">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-4 control-label" for="prof_gender">Jenis Kelamin</label>
				<div class="col-md-8">
					<label class="radio-inline" for="pria"><input name="prof_gender" value="Laki-laki" id="pria" <?php if($prof->gender == 'Laki-laki') {echo 'checked="checked"';} ?> type="radio">Laki-laki</label>
					<label class="radio-inline" for="wanita"><input name="prof_gender" value="Perempuan" id="wanita" <?php if($prof->gender == 'Perempuan') {echo 'checked="checked"';} ?> type="radio">Perempuan</label>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-4 control-label" for="prof_home_addr">Alamat Rumah *</label>
				<div class="col-md-8">
					<input name="prof_home_addr" value="<?php echo set_value('prof_home_addr', $prof->home_addr); ?>" class="sm-form-control required" type="text">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-4 control-label" for="prof_home_city">Kab./Kota *</label>
				<div class="col-md-8">
					<input name="prof_home_city" value="<?php echo set_value('prof_home_city', $prof->home_city); ?>" class="sm-form-control required" type="text">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-4 control-label" for="prof_home_prov">Provinsi *</label>
				<div class="col-md-8">
				<?php echo form_dropdown('prof_home_prov', $propinsi, set_value('prof_home_prov'), 'class="sm-form-control required"') ?>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-4 control-label" for="prof_phone">Nomor Ponsel</label>
				<div class="col-md-8">
					<input name="prof_phone" value="<?php echo set_value('prof_phone', $prof->phone); ?>" class="sm-form-control" type="text">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-4 control-label" for="prof_ahli">Bidang Keahlian ( Yang Dikuasai) </label>
				<div class="col-md-8">
					<input name="prof_ahli" value="<?php echo $prof->ahli_bid; ?>" class="sm-form-control" type="text">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-4 control-label" for="prof_web">Website ( Jika ada)</label>
				<div class="col-md-8">
					<input name="prof_web" value="<?php echo $prof->website; ?>" class="sm-form-control" type="text">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-4 control-label" for="prof_fb">Facebook</label>
				<div class="col-md-8">
					<input name="prof_fb" value="<?php echo $prof->socmed_fb; ?>" class="sm-form-control" type="text">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-4 control-label" for="prof_tw">Twitter</label>
				<div class="col-md-8">
					<input name="prof_tw" value="<?php echo $prof->socmed_tw; ?>" class="sm-form-control" type="text">
				</div>
			</div>

		<!-- FORM BIODATA END -->
	</div>

	<div class="panel-footer">
		<div class="text-right">
			<div class="btn-group" role="group" aria-label="Button Group">
				<button type="submit" class="button button-3d button-black" id="form-submit" name="submit" value="submit">Simpan</button>
			</div>
		</div>		
	</div>
</div>
</form>

<script type="text/javascript">
	$(document).ready(function () {
		$('head').append('<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.datepick.css">');
	});
	
	var s = document.createElement("script");
	s.type = "text/javascript";
	s.src = "<?php echo base_url(); ?>assets/js/jquery.plugin.js";
	$("head").append(s);
	
	var s = document.createElement("script");
	s.type = "text/javascript";
	s.src = "<?php echo base_url(); ?>assets/js/jquery.datepick.js";
	$("head").append(s);
</script>

<script type="text/javascript">
$("input:checkbox").on('click', function() {
// in the handler, 'this' refers to the box clicked on
	var $box = $(this);
	if ($box.is(":checked")) {
		// the name of the box is retrieved using the .attr() method
		// as it is assumed and expected to be immutable
		var group = "input:checkbox[name='" + $box.attr("name") + "']";
		// the checked state of the group/box on the other hand will change
		// and the current value is retrieved using .prop() method
		$(group).prop("checked", false);
		$box.prop("checked", true);
	} else {
		$box.prop("checked", false);
	}
});

$('input[id=lefile]').change(function() {
$('#prof_avatar').val($(this).val());
});
</script>

<script>
$(function() {
	$('#birthdate').datepick({
		dateFormat: 'yyyy-mm-dd',
		minDate: new Date(1940, 1-1, 1),
		maxDate: new Date(2000, 12-1, 31),
		yearRange: '1940:2000'
	});
});
</script>

<script type="text/javascript">
	jQuery("form").validate({
		submitHandler: function(form) {
			// jQuery('.form-process').fadeIn();
			jQuery(form).ajaxSubmit({
				target: '#job-form-result',
				success: function() {
					// jQuery('.form-process').fadeOut();
					jQuery(form).find('.sm-form-control').val('');
					// jQuery('#job-form-result').attr('data-notify-msg', jQuery('#job-form-result').html()).html('');
					SEMICOLON.widget.notifications(jQuery('#job-form-result'));
				}
			});
		}
	});
</script>

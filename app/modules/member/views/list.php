<?php if(count($member) > 0): ?>
<section class="container clearfix topmargin">
    <div class="postcontent nobottommargin clearfix">
        <div id="posts" class="small-thumbs">
            <div class="row flex">
            <?php foreach ($member as $list): ?>
                <div class="col-md-6">
                    <div class="panel panel-default noshadow" >
                        <div class="panel-heading"><?php echo $list->realname ?></div>
                        <div class="panel-body">
                            <div class="col-sm-8">
                                <ul class="iconlist" style="line-height: 2em">
                                    <?php echo ($list->ahli_bid == '') ? '' : '<li><i class="icon-check"></i> '.$list->ahli_bid . '</li>' ?>                                        
                                    <?php echo ($list->socmed_fb == '') ? '' : '<li><i class="icon-check"></i> '.$list->socmed_fb . '</li>' ?>                                        
                                    <?php echo ($list->socmed_tw == '') ? '' : '<li><i class="icon-check"></i> '.$list->socmed_tw . '</li>' ?>                                        
                                </ul>
                            </div>
                            <div class="col-sm-4">
                                <img src="<?php echo base_url() . PERSON . '/person.png' ?>" alt="<?php echo $list->realname ?>">                                
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
            </div>
        <?php echo $pagination ?>
        </div>
    </div>
    <?php $this->load->view('archives/sidebar') ?>
</section>
<?php else: ?>
<?php //show_404() ?>
<?php endif ?>
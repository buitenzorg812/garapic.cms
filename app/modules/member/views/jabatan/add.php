		<div class="panel panel-default noshadow">
			<div class="panel-body">
				<?php echo form_open(current_url(), 'class="form-horizontal nobottommargin" id="biodata-form" name="biodata-form"') ?>
				<?php if($custom_error == '') : ?>
				<h4 style="text-transform: uppercase">Riwayat Pekerjaan</h4>
				Mohon lengkapi formulir riwayat jabatan berikut ini.
				<hr />
				<?php else: ?>
					<?php $this->load->view('submit_error') ?>
				<?php endif; ?>
				<?php if($success == '') : ?>
				<?php else: ?>
					<?php $this->load->view('submit_success') ?>
				<?php endif; ?>

				<div class="form-group">
					<label class="col-md-4 control-label" for="prof_jab_nama">Jabatan</label>
					<div class="col-md-8">
						<input name="prof_jab_nama" value="<?php echo set_value('prof_jab_nama', $jab->jbt_type) ?>" class="sm-form-control" type="text">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label" for="prof_jab_office">Nama Kantor</label>
					<div class="col-md-8">
						<input name="prof_jab_office" value="<?php echo set_value('prof_jab_office',$jab->jbt_office) ?>" class="sm-form-control" type="text">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label" for="prof_jab_periode">Periode Bekerja</label>
					<div class="col-md-8">
						<input name="prof_jab_periode" value="<?php echo set_value('prof_jab_periode',$jab->jbt_periode) ?>" class="sm-form-control" type="text">
					</div>
				</div>

				<div class="form-group nobottommargin">
					<?php echo form_hidden('member_id', $jab->member_id);?>
					<?php echo form_hidden('jabatan_id', $jab->jabatan_id);?>
					<div class="text-right col-md-12">
						<div id="button1idGroup" class="btn-group" role="group" aria-label="Button Group">
							<button type="submit" class="button button-rounded button-black" id="form-submit" name="submit" value="submit">Simpan</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>


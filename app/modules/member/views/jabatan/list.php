<?php if(count($jabatan) > 0) : ?>
<?php foreach($jabatan as $jab) : ?>
<div class="panel panel-default noshadow clearfix">
	<div class="panel-heading">
		<div class="panel-title"><?php echo $jab->jbt_type ?></div>
	</div>
	<table class="table table-bordered">
		<tr>
			<td class="text-right" width="30%">Kantor</td>
			<td><?php echo $jab->jbt_office ?></td>
		</tr>
		<tr>
			<td class="text-right" width="30%">Periode</td>
			<td><?php echo $jab->jbt_periode ?></td>
		</tr>
	</table>
	<div class="panel-body">
		<div class="text-right">
			<a href="<?php echo site_url('member/jabatan/remove/'.$jab->jabatan_id) ?>" class="btn btn-sm btn-default remove">Hapus</a> 
			<a href="<?php echo site_url('member/jabatan/edit/'.$jab->jabatan_id) ?>" class="btn btn-sm btn-default">Ubah</a>
		</div>		
	</div>
</div>
<?php endforeach ?>
<?php else : ?>
<h4 class="nomargin">Belum ada data</h4>
<p>Silahkan klik pada tautan dibawah ini untuk menambahkan informasi baru.</p>
<?php endif ?>
<p>
	<a href="<?php echo site_url('member/jabatan/add') ?>" class="button button-small button-rounded button-black">Tambah</a>
</p>

<script>
	$('.remove').click(function(){
		if(confirm('Anda yakin menghapus data ini ? ')) {
			return true;
		} else {
			return false;
		}
	});
</script>
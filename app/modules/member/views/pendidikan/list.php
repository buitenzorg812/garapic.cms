<?php if(count($education) > 0) : ?>
<?php foreach($education as $edu) : ?>
<div class="panel panel-default noshadow clearfix">
	<div class="panel-heading">
		<div class="panel-title"><?php echo $edu->sarjana ?></div>
	</div>
	<table class="table table-bordered">
		<tr>
			<td class="text-right" width="30%">Fakultas</td>
			<td><?php echo $edu->jurusan ?></td>
		</tr>
		<tr>
			<td class="text-right" width="30%">Universitas</td>
			<td><?php echo $edu->kampus ?></td>
		</tr>
		<tr>
			<td class="text-right" width="30%">Lulus Tahun</td>
			<td><?php echo $edu->lulus ?></td>
		</tr>
		<tr>
			<td class="text-right" width="30%">Judul Tugas Akhir</td>
			<td><?php echo $edu->skripsi ?></td>
		</tr>
	</table>
	<div class="panel-body">
		<div class="text-right">
			<a href="<?php echo site_url('member/pendidikan/remove/'.$edu->edu_id) ?>" class="btn btn-sm btn-default remove">Hapus</a> 
			<a href="<?php echo site_url('member/pendidikan/edit/'.$edu->edu_id) ?>" class="btn btn-sm btn-default">Ubah</a>
		</div>		
	</div>
</div>
<?php endforeach ?>
<?php else : ?>
<h4 class="nomargin">Belum ada data</h4>
<p>Silahkan klik pada tautan dibawah ini untuk menambahkan informasi baru.</p>
<?php endif ?>
<p>
	<a href="<?php echo site_url('member/pendidikan/add') ?>" class="button button-small button-rounded button-black">Tambah</a>
</p>

<script>
	$('.remove').click(function(){
		if(confirm('Anda yakin menghapus data ini ? ')) {
			return true;
		} else {
			return false;
		}
	});
</script>
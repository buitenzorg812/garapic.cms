		<div class="panel panel-default noshadow">
			<div class="panel-body">
				<?php echo form_open(current_url(), 'class="form-horizontal nobottommargin" id="biodata-form" name="biodata-form"') ?>
				<?php if($custom_error == '') : ?>
				<h4 class="nomargin" style="text-transform: uppercase">Data Pendidikan</h4>
				Mohon lengkapi formulir riwayat pendidikan anggota berikut ini.
				<hr />
				<?php else: ?>
					<?php $this->load->view('submit_error') ?>
				<?php endif; ?>
				<?php if($success == '') : ?>
				<?php else: ?>
					<?php $this->load->view('submit_success') ?>
				<?php endif; ?>

				<div class="form-group">
					<label class="col-md-4 control-label" for="prof_edu_level">Tingkat Pendidikan</label>
					<div class="col-md-8">
						<select name="prof_edu_level" class="sm-form-control">
							<option value="Belum diketahui" style="margin-bottom: 10px;">Pilih Salah Satu</option>
							<option <?php if($edu->sarjana == 'SMA/SMK'){ echo 'selected = "selected"'; } ?> value="SMA/SMK">SMA/SMK</option>
							<option <?php if($edu->sarjana == 'Diploma'){ echo 'selected = "selected"'; } ?> value="Diploma">Diploma</option>
							<option <?php if($edu->sarjana == 'Sarjana'){ echo 'selected = "selected"'; } ?> value="Sarjana">Sarjana</option>
							<option <?php if($edu->sarjana == 'Magister'){ echo 'selected = "selected"'; } ?> value="Magister">Magister</option>
							<option <?php if($edu->sarjana == 'Doktoral'){ echo 'selected = "selected"'; } ?> value="Doktoral">Doktoral</option>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label" for="prof_edu_jur">Fakultas/Jurusan</label>
					<div class="col-md-8">
						<input name="prof_edu_jur" value="<?php echo set_value('prof_edu_jur', $edu->jurusan) ?>" class="sm-form-control" type="text">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label" for="prof_edu_camp">Kampus/Almamater</label>
					<div class="col-md-8">
						<input name="prof_edu_camp" value="<?php echo set_value('prof_edu_camp',$edu->kampus) ?>" class="sm-form-control" type="text">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label" for="prof_edu_year">Tahun Lulus</label>
					<div class="col-md-8">
						<input name="prof_edu_year" value="<?php echo set_value('prof_edu_year',$edu->lulus) ?>" class="sm-form-control" type="text">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label" for="prof_edu_title">Judul Skripsi/Tesis/Disertasi</label>
					<div class="col-md-8">
						<input name="prof_edu_title" value="<?php echo set_value('prof_edu_title',$edu->skripsi) ?>" class="sm-form-control" type="text">
					</div>
				</div>

				<div class="form-group nobottommargin">
					<?php echo form_hidden('member_id', $edu->member_id);?>
					<?php echo form_hidden('edu_id', $edu->edu_id);?>
					<div class="text-right col-md-12">
						<div id="button1idGroup" class="btn-group" role="group" aria-label="Button Group">
							<button type="submit" class="button button-rounded button-black" id="form-submit" name="submit" value="submit">Simpan</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>


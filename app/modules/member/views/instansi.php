<?php echo validation_errors() ?>
<?php echo form_open(current_url(), 'class="form-horizontal nobottommargin" id="biodata-form" name="biodata-form"') ?>
<?php echo form_hidden('member_id', $prof->member_id);?>
<div class="panel panel-default noshadow">
	<div class="panel-heading">
		<h3 class="nobottommargin" style="text-transform: uppercase;">Instansi</h3>
	</div>
	<div class="panel-body">
		<!-- FORM BIODATA BEGIN -->
			Informasi instansi tempat anda bekerja saat ini.
			<hr />

            <div class="form-group">
                <label class="col-md-4 control-label" for="prof_offc_name">Nama Instansi</label>
                <div class="col-md-8">
                    <input name="prof_offc_name" value="<?php echo set_value('prof_offc_name', $inst->office_name); ?>" class="sm-form-control required" type="text">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="prof_offc_addr">Alamat Instansi</label>
                <div class="col-md-8">
                    <input name="prof_offc_addr" value="<?php echo set_value('prof_offc_addr', $inst->office_addr); ?>" class="sm-form-control required" type="text">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="prof_offc_city">Kab./Kota</label>
                <div class="col-md-8">
                    <input name="prof_offc_city" value="<?php echo set_value('prof_offc_city', $inst->office_city); ?>" class="sm-form-control required" type="text">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="prof_offc_city">Propinsi</label>
                <div class="col-md-8">
				<?php echo form_dropdown('prof_offc_prov', $propinsi, set_value('prof_offc_prov', $inst->office_prov), 'class="sm-form-control required"') ?>
                </div>
            </div>

		<!-- FORM BIODATA END -->
	</div>

	<div class="panel-footer">
		<div class="text-right">
			<div class="btn-group" role="group" aria-label="Button Group">
				<button type="submit" class="button button-3d button-black" id="form-submit" name="submit" value="submit">Simpan</button>
			</div>
		</div>		
	</div>
</div>
</form>


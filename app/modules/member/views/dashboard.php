<section class="container topmargin bottommargin clearfix">
	<div class="row">
		<div class="col-md-3">
			<div class="panel panel-default noshadow">				
				<div class="panel-body">				
					<img src="<?php if($prof->avatar == NULL){ echo base_url(). PERSON . '/person.png'; } else { echo base_url(). PERSON .'/'.$avatar->avatar; } ?>" class="img-thumb" />
				</div>
			</div>
            <div class="list-group">
                <?php
                $menus = array('profile','instansi','pendidikan','jabatan','publikasi','penelitian','pengabdian','logout');                
                foreach($menus as $menu) : ?>
                <a href="<?php echo site_url('member/'.$menu); ?>"  class="list-group-item <?php echo ($menu == $this->uri->segment(2)) ? 'list-group-item-info' : '' ?>"><?php echo ucfirst($menu) ?></a>
                <?php endforeach ?>            
            </div>
		</div>
		<div class="col-md-9">
		<?php echo $member_content ?>
		</div>
	</div>
</section>
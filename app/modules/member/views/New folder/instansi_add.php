<section class="container clearfix">
	<div class="row">
		<div class="panel panel-default noshadow">
			<div class="panel-heading nobottompadding mbrMenu">
				<ul class="nav nav-tabs">
					<li><a href="<?php echo base_url(); ?>dashboard">Dashboard</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/profile">Profile</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/biodata">Biodata</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/instansi">Instansi</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/pendidikan">Pendidikan</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/jabatan">Jabatan</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/publikasi">Publikasi</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/penelitian">Riset</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/pengabdian">Pengabdian</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/logout">Logout</a></li>
				</ul>
			</div>
			<div class="panel-body norightpadding noleftpadding">
				<div class="tab-content">
					<div class="col-md-4 panel nobottommargin">
<?php $this->load->view('avatar'); ?>
					</div>
					<div class="col-md-8 panel nobottommargin">
<?php foreach ($memBio as $bio): ?>
						<!-- FORM INSTANSI BEGIN -->
						<?php echo form_open(base_url('dashboard/instansi'), 'class="form-horizontal nobottommargin" id="biodata-form" name="biodata-form"') ?>
							<?php if($custom_error == '') : ?>
							<h4 class="nomargin">Data Instansi</h4>
							Mohon lengkapi formulir instansi tempat anggota bekerja berikut ini.
							<hr />
							<?php else: ?>
								<?php $this->load->view('submit_error') ?>
							<?php endif; ?>
							<?php if($success == '') : ?>
							<?php else: ?>
								<?php $this->load->view('submit_success') ?>
							<?php endif; ?>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_offc_name">Nama Instansi</label>
								<div class="col-md-8">
									<input name="prof_offc_name" value="<?php echo $bio->office_name; ?>" class="form-control input-md" type="text">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_offc_addr">Alamat Instansi</label>
								<div class="col-md-8">
									<input name="prof_offc_addr" value="<?php echo $bio->office_addr; ?>" class="form-control input-md" type="text">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_offc_city">Kab./Kota</label>
								<div class="col-md-8">
									<input name="prof_offc_city" value="<?php echo $bio->office_city; ?>" class="form-control input-md" type="text">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_offc_prov">Provinsi</label>
								<div class="col-md-8">
									<select name="prof_offc_prov" class="form-control">
										<option value="Belum diketahui" style="margin-bottom: 10px;">PILIH SALAH SATU</option>
										<option <?php if($bio->office_prov == 'Bangka-Belitung'){ echo 'selected = "selected"'; } ?> value="Bangka-Belitung">BANGKA-BELITUNG</option>
										<option <?php if($bio->office_prov == 'Bali'){ echo 'selected = "selected"'; } ?> value="Bali">BALI</option>
										<option <?php if($bio->office_prov == 'Banten'){ echo 'selected = "selected"'; } ?> value="Banten">BANTEN</option>
										<option <?php if($bio->office_prov == 'Bengkulu'){ echo 'selected = "selected"'; } ?> value="Bengkulu">BENGKULU</option>
										<option <?php if($bio->office_prov == 'DI Yogyakarta'){ echo 'selected = "selected"'; } ?>  value="DI Yogyakarta">DI YOGYAKARTA</option>
										<option <?php if($bio->office_prov == 'DKI Jakarta'){ echo 'selected = "selected"'; } ?> value="DKI Jakarta">DKI JAKARTA</option>
										<option <?php if($bio->office_prov == 'Gorontalo'){ echo 'selected = "selected"'; } ?> value="Gorontalo">GORONTALO</option>
										<option <?php if($bio->office_prov == 'Jambi'){ echo 'selected = "selected"'; } ?> value="Jambi">JAMBI</option>
										<option <?php if($bio->office_prov == 'Jawa Barat'){ echo 'selected = "selected"'; } ?> value="Jawa Barat">JAWA BARAT</option>
										<option <?php if($bio->office_prov == 'Jawa Tengah'){ echo 'selected = "selected"'; } ?> value="Jawa Tengah">JAWA TENGAH</option>
										<option <?php if($bio->office_prov == 'Jawa Timur'){ echo 'selected = "selected"'; } ?> value="Jawa Timur">JAWA TIMUR</option>
										<option <?php if($bio->office_prov == 'Kalimantan Barat'){ echo 'selected = "selected"'; } ?> value="Kalimantan Barat">KALIMANTAN BARAT</option>
										<option <?php if($bio->office_prov == 'Kalimantan Selatan'){ echo 'selected = "selected"'; } ?> value="Kalimantan Selatan">KALIMANTAN SELATAN</option>
										<option <?php if($bio->office_prov == 'Kalimantan Tengah'){ echo 'selected = "selected"'; } ?> value="Kalimantan Tengah">KALIMANTAN TENGAH</option>
										<option <?php if($bio->office_prov == 'Kalimantan Timur'){ echo 'selected = "selected"'; } ?> value="Kalimantan Timur">KALIMANTAN TIMUR</option>
										<option <?php if($bio->office_prov == 'Kepulauan Riau'){ echo 'selected = "selected"'; } ?> value="Kepulauan Riau">KEPULAUAN RIAU</option>
										<option <?php if($bio->office_prov == 'Lampung'){ echo 'selected = "selected"'; } ?> value="Lampung">LAMPUNG</option>
										<option <?php if($bio->office_prov == 'Maluku'){ echo 'selected = "selected"'; } ?> value="Maluku">MALUKU</option>
										<option <?php if($bio->office_prov == 'Maluku Utara'){ echo 'selected = "selected"'; } ?> value="Maluku Utara">MALUKU UTARA</option>
										<option <?php if($bio->office_prov == 'Nangroe Aceh Darussalam'){ echo 'selected = "selected"'; } ?> value="Nangroe Aceh Darussalam">NANGGROE ACEH DARUSSALAM</option>
										<option <?php if($bio->office_prov == 'Nusa Teggara Timur'){ echo 'selected = "selected"'; } ?> value="Nusa Teggara Timur">NUSA TEGGARA TIMUR</option>
										<option <?php if($bio->office_prov == 'Nusa Tenggara Barat'){ echo 'selected = "selected"'; } ?> value="Nusa Tenggara Barat">NUSA TENGGARA BARAT</option>
										<option <?php if($bio->office_prov == 'Papua'){ echo 'selected = "selected"'; } ?> value="Papua">PAPUA</option>
										<option <?php if($bio->office_prov == 'Papua Barat'){ echo 'selected = "selected"'; } ?> value="Papua Barat">PAPUA BARAT</option>
										<option <?php if($bio->office_prov == 'Riau'){ echo 'selected = "selected"'; } ?> value="Riau">RIAU</option>
										<option <?php if($bio->office_prov == 'Sulawesi Barat'){ echo 'selected = "selected"'; } ?> value="Sulawesi Barat">SULAWESI BARAT</option>
										<option <?php if($bio->office_prov == 'Sulawesi Selatan'){ echo 'selected = "selected"'; } ?> value="Sulawesi Selatan">SULAWESI SELATAN</option>
										<option <?php if($bio->office_prov == 'Sulawesi Tengah'){ echo 'selected = "selected"'; } ?> value="Sulawesi Tengah">SULAWESI TENGAH</option>
										<option <?php if($bio->office_prov == 'Sulawesi Tenggara'){ echo 'selected = "selected"'; } ?> value="Sulawesi Tenggara">SULAWESI TENGGARA</option>
										<option <?php if($bio->office_prov == 'Sulawesi Utara'){ echo 'selected = "selected"'; } ?> value="Sulawesi Utara">SULAWESI UTARA</option>
										<option <?php if($bio->office_prov == 'Sumatera Barat'){ echo 'selected = "selected"'; } ?> value="Sumatera Barat">SUMATERA BARAT</option>
										<option <?php if($bio->office_prov == 'Sumatera Selatan'){ echo 'selected = "selected"'; } ?> value="Sumatera Selatan">SUMATERA SELATAN</option>
										<option <?php if($bio->office_prov == 'Sumatera Utara'){ echo 'selected = "selected"'; } ?> value="Sumatera Utara">SUMATERA UTARA</option>
									</select>
								</div>
							</div>

							<div class="form-group nobottommargin">
								<?php echo form_hidden('member_id', $member_id);?>
								<div class="text-right col-md-12">
									<div id="button1idGroup" class="btn-group" role="group" aria-label="Button Group">
										<button type="submit" class="btn btn-primary nomargin" id="form-submit" name="submit" value="submit">Simpan</button>
									</div>
								</div>
							</div>
						</form>
						<!-- FORM INSTANSI END -->
					</div>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>

<section class="container clearfix topmargin">
    <div class="col_two_third clearfix">
        <div class="panel panel-default noshadow">
            <div class="panel-heading">
                <h3 class="nobottommargin" style="text-transform: uppercase;">Konfirmasi Pendaftaran</h3>
            </div>
            <div class="panel-body">
                <p class="nobottommargin">
                <?php echo $message ?>
                </p>
            </div>
        </div>
    </div>
    <?php $this->load->view('sidebar') ?>
</section>
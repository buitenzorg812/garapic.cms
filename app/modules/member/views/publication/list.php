<?php if(count($publication) > 0) : ?>
<?php foreach($publication as $pub) : ?>
<div class="panel panel-default noshadow clearfix">
	<div class="panel-heading">
		<div class="panel-title"><?php echo $pub->pub_title ?></div>
	</div>
	<div class="panel-body">
		<?php echo $pub->pub_abstract ?>
	</div>
	<table class="table table-bordered">
		<tr>
			<td class="text-right" width="30%">Penulis Tambahan</td>
			<td><?php echo $pub->pub_partner ?></td>
		</tr>
		<tr>
			<td class="text-right" width="30%">Jenis</td>
			<td><?php echo $pub->pub_type ?></td>
		</tr>
		<tr>
			<td class="text-right" width="30%">ISBN</td>
			<td><?php echo $pub->pub_isbn ?></td>
		</tr>
		<tr>
			<td class="text-right" width="30%">Tahun</td>
			<td><?php echo $pub->pub_year ?></td>
		</tr>
		<tr>
			<td class="text-right" width="30%">Berkas Download</td>
			<td><a target="_blank" href="<?php echo site_url('member/publikasi/downld/' . $pub->pub_id) ?>"><?php echo $pub->pub_file ?></a></td>
		</tr>
		<tr>
			<td class="text-right" width="30%">Tautan</td>
			<td><a href="<?php echo $pub->pub_link ?>" target="_blank"><?php echo $pub->pub_link ?></a></td>
		</tr>
	</table>
	<div class="panel-body">
		<div class="text-right">
			<a href="<?php echo site_url('member/publikasi/remove/'.$pub->pub_id) ?>" class="btn btn-sm btn-default remove">Hapus</a> 
			<a href="<?php echo site_url('member/publikasi/edit/'.$pub->pub_id) ?>" class="btn btn-sm btn-default">Ubah</a>
		</div>		
	</div>
</div>
<?php endforeach ?>
<?php else : ?>
<h4 class="nomargin">Belum ada data</h4>
<p>Silahkan klik pada tautan dibawah ini untuk menambahkan informasi baru.</p>
<?php endif ?>
<p>
	<a href="<?php echo site_url('member/publikasi/add') ?>" class="button button-small button-rounded button-black">Tambah</a>
</p>

<script>
	$('.remove').click(function(){
		if(confirm('Anda yakin menghapus data ini ? ')) {
			return true;
		} else {
			return false;
		}
	});
</script>
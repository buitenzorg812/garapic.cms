		<div class="panel panel-default noshadow">
			<div class="panel-body">
				<?php echo form_open(current_url(), 'class="form-horizontal nobottommargin" id="biodata-form" name="biodata-form"') ?>
				<?php if($custom_error == '') : ?>
				<h4 style="text-transform: uppercase">Data Penelitian</h4>
				Mohon lengkapi formulir riwayat penelitian berikut ini.
				<hr />
				<?php else: ?>
					<?php $this->load->view('submit_error') ?>
				<?php endif; ?>
				<?php if($success == '') : ?>
				<?php else: ?>
					<?php $this->load->view('submit_success') ?>
				<?php endif; ?>

				<div class="form-group">
					<label class="col-md-4 control-label" for="prof_riset_title">Judul</label>
					<div class="col-md-8">
						<input name="prof_riset_title" value="<?php echo set_value('prof_riset_title', $ris->riset_title) ?>" class="sm-form-control" type="text">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label" for="prof_riset_part">Peneliti Lain ( jika ada )</label>
					<div class="col-md-8">
						<input name="prof_riset_part" value="<?php echo set_value('prof_riset_part',$ris->riset_partner) ?>" class="sm-form-control" type="text">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label" for="prof_riset_year">Tahun</label>
					<div class="col-md-8">
						<input name="prof_riset_year" value="<?php echo set_value('prof_riset_year',$ris->riset_year) ?>" class="sm-form-control" type="text">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label" for="prof_riset_fund">Sumber Dana</label>
					<div class="col-md-8">
						<input name="prof_riset_fund" value="<?php echo set_value('prof_riset_fund',$ris->riset_fund) ?>" class="sm-form-control" type="text">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label" for="prof_riset_stake">Stakeholder</label>
					<div class="col-md-8">
						<input name="prof_riset_stake" value="<?php echo set_value('prof_riset_stake',$ris->riset_stake) ?>" class="sm-form-control" type="text">
					</div>
				</div>

				<div class="form-group nobottommargin">
					<?php echo form_hidden('member_id', $ris->member_id);?>
					<?php echo form_hidden('riset_id', $ris->riset_id);?>
					<div class="text-right col-md-12">
						<div id="button1idGroup" class="btn-group" role="group" aria-label="Button Group">
							<button type="submit" class="button button-rounded button-black" id="form-submit" name="submit" value="submit">Simpan</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>


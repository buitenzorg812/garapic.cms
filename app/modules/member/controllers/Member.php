<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends MX_Controller {

    public $data            = array();
    private $url            = 'member';
    private $perpage        = 6;
    private $member_mail    = '';
    private $propinsi    = '';
    
    public function __construct() 
    {   
        parent::__construct();

        // load model
        $this->load->model(array('auth_model','crudmod'));

        // set default value
        $this->data['title']    = 'Data Anggota';

        // Set default language at beginning
        $this->session->set_userdata(array('lang' => 'id'));
        $this->data['propinsi']       = array(
                                    '11' => 'Aceh',
                                    '12' => 'Sumatera Utara',
                                    '13' => 'Sumatera Barat',
                                    '14' => 'Riau',
                                    '15' => 'Jambi',
                                    '16' => 'Sumatera Selatan',
                                    '17' => 'Bengkulu',
                                    '18' => 'Lampung',
                                    '19' => 'Kepulauan Bangka Belitung',
                                    '21' => 'Kepulauan Riau',
                                    '31' => 'Dki Jakarta',
                                    '32' => 'Jawa Barat',
                                    '33' => 'Jawa Tengah',
                                    '34' => 'Di Yogyakarta',
                                    '35' => 'Jawa Timur',
                                    '36' => 'Banten',
                                    '51' => 'Bali',
                                    '52' => 'Nusa Tenggara Barat',
                                    '53' => 'Nusa Tenggara Timur',
                                    '61' => 'Kalimantan Barat',
                                    '62' => 'Kalimantan Tengah',
                                    '63' => 'Kalimantan Selatan',
                                    '64' => 'Kalimantan Timur',
                                    '65' => 'Kalimantan Utara',
                                    '71' => 'Sulawesi Utara',
                                    '72' => 'Sulawesi Tengah',
                                    '73' => 'Sulawesi Selatan',
                                    '74' => 'Sulawesi Tenggara',
                                    '75' => 'Gorontalo',
                                    '76' => 'Sulawesi Barat',
                                    '81' => 'Maluku',
                                    '82' => 'Maluku Utara',
                                    '91' => 'Papua Barat',
                                    '94' => 'Papua');

        // for debug purpose only
        $this->output->enable_profiler(false);

    }

    public function index($offset = 0)
    {
        // pagination 
        $this->db
        ->select('member_id')
        ->from('member');
        $query = $this->db->get();
        $count                      = $query->num_rows();
        $config['per_page']         = $this->perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url($this->url), $count, $this->perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        // get all data
        $this->db
        ->from('member')
        ->limit($this->perpage, $offset );
        $query = $this->db->get();

        // if not empty
        if($query->num_rows() > 0) {

            // most view
            $this->data['view_most']      = $this->media->blog_view_most();
            $this->data['view_latest']    = $this->media->blog_view_latest();

            // galeri
            $this->data['gallery']        = $this->media->images_list(4, 5); // $cat_id, $limit
            $this->data['footer_gallery'] = $this->media->images_list(4, 6); // $cat_id, $limit

            // generate output
            $this->data['member']       = $query->result();
            $this->data['content']      = $this->load->view('list', $this->data, true);
 
        } else {
 
            // if no data
            $this->data['message']      = '<span>Silahkan kunjungi laman lainnya atau kembali ke <a href="<?php echo base_url() ?>">beranda</a>.</span>';
            $this->data['content']      = $this->load->view('master/blank', $this->data, true);                    
 
        }

        // Generate output
        $this->load->view('master/content', $this->data);
    }

    // Add a new item
    public function register()
    {
        //Validation config
        $config = array(
            array(
                'field'     => 'reg_name',
                'label'     => 'Nama Lengkap',
                'rules'     => 'required',
                'errors'    => array(
                    'required'      => 'Mohon isikan dengan nama lengkap anda.'
                ),
            ),
            array(
                'field'     => 'reg_premail',
                'label'     => 'Email',
                'rules'     => 'required|valid_email',
                'errors'    => array(
                    'required'      => 'Mohon isikan alamat email anda.',
                    'valid_email'   => 'Format email belum benar.'
                ),
            ),
            array(
                'field'     => 'reg_mail',
                'label'     => 'Konfirmasi Email',
                'rules'     => 'required|valid_email|matches[reg_premail]|is_unique[member.email]',
                'errors'    => array(
                    'required'      => 'Mohon isikan alamat email anda.',
                    'valid_email'   => 'Format email belum benar.',
                    'matches'       => '<strong>Konfirmasi Email</strong> berbeda dengan alamat <strong>Email</strong>.',
                    'is_unique'     => '<strong style="color:red">Alamat email anda sudah pernah terdaftar.</strong>'
                ),
            )
        );

        $this->form_validation->set_rules($config);
        $data['custom_error']   = '';
        $view['title']              = 'Pendaftaran';
                    
            if ($this->form_validation->run() == FALSE)
            {
                // If Failed
                $view['title']          = 'Pendaftaran';
                $data['custom_error']   = validation_errors('<li><i class="icon-line-square-check"></i> ', '</li>');
                $view['content']        = $this->load->view('register', '', TRUE);                

            } else {

                // If Success
                $r_name                 = $this->input->post('reg_name');
                $r_mail                 = $this->input->post('reg_mail');
                
                $clean                  = $this->security->xss_clean($this->input->post(NULL, TRUE));
                $id                     = $this->auth_model->insertMember($clean);
                $token                  = $this->auth_model->insertToken($id);
                
                if($id) {
                    // $config = Array(
                    //   'protocol'    => 'smtp',
                    //   'smtp_host'   => 'mailtrap.io',
                    //   'smtp_port'   => 2525,
                    //   'smtp_user'   => '34458fbe32e160c12',
                    //   'smtp_pass'   => 'cef81eb13c8131',
                    //   'mailtype'    => 'html',
                    //   'crlf'        => "\r\n",
                    //   'newline'     => "\r\n"
                    // );

                    $config = Array(
                      'protocol'    => 'smtp',
                      'smtp_host'   => 'mail.garapic.com',
                      'smtp_port'   => 25,
                      'smtp_user'   => 'seminar@garapic.com',
                      'smtp_pass'   => 'e7d200e7d200',
                      'crlf'        => "\r\n",
                      'newline'     => "\r\n",
                      'mailtype'    => "html"
                    );

                    $this->load->library('email', $config);                     
                    
                    $qstring    = base64_encode($token);
                    $url        = base_url() . 'member/complete/token/' . $qstring;
                    $link       = '<a href="' . $url . '">' . $url . '</a>';
                    
                    $subject    = 'Konfirmasi Pendaftaran';
                    $message    = '';
                    $message    = "Hai  ".$r_name.",<br /><br />Silahkan klik pada tautan berikut untuk proses verifikasi data anda.<br /> " .$link;
                    $message   .= "<br><br><br>Hormat kami,<br>Pengurus Ikatan Geografi Indonesia";
                    $this->email->from('conigter@gmail.com', 'Ikatan Geografi Indonesia');
                    $this->email->to($r_mail);
                    $this->email->subject($subject);
                    $this->email->message($message);
                    if($this->email->send()) {
                        $view['message']            = '<p>Kami telah mengirimkan email konfirmasi ke alamat email anda.</p>';
                    } else {
                        
                    }

                    $view['footer_gallery']     = $this->media->images_list(4, 6);
                    $view['title']              = 'Konfirmasi Pendaftaran';
                    $view['content']            = $this->load->view('submit_success', $view, TRUE);
                }            
        }
        $view['footer_gallery'] = $this->media->images_list(4, 6);
        $this->load->view('master/content', $view);        
    }

    public function complete()
    {
        $token      = base64_decode($this->uri->segment(4));
        $cleanToken = $this->security->xss_clean($token);
        $user_info  = $this->auth_model->isTokenValid($cleanToken);

        if($user_info->status == 'approved'){
            $data['custom_error']   = '<li><i class="icon-line-square-check"></i> Token is invalid or expired</li>';
            $view['title']          = 'Token Kadaluarsa';
            $view['message']        = 'Akun anda mungkin telah diaktifkan atau token telah kadaluarsa.';
            $view['content']        = $this->load->view('master/blank', $view, true);
        } else {        
            $data = array(
                'realName'  => $user_info->realname,
                'email'     => $user_info->email,
                'member_id' => $user_info->member_id,
                'token'     => base64_encode($token)
            );
        
            //Validation config
            $config = array(
                array(
                    'field'     => 'reg_pass',
                    'label'     => 'Password',
                    'rules'     => 'required|min_length[6]',
                    'errors'    => array(
                        'required'      => 'Mohon isikan password anda.',
                        'min_length'    => 'Panjang password minimal 6 karakter.'
                    ),
                ),
                array(
                    'field'     => 'reg_passed',
                    'label'     => 'Konfirmasi Password',
                    'rules'     => 'required|min_length[6]|matches[reg_pass]',
                    'errors'    => array(
                        'required'      => 'Mohon isikan password anda.',
                        'min_length'    => 'Panjang password minimal 6 karakter.',
                        'matches'       => '<strong>Konfirmasi Password</strong> harus sama dengan Password.'
                    ),
                )
            );

            $this->form_validation->set_rules($config);
            $data['custom_error']   = '';
        
            if ($this->form_validation->run() == FALSE) {
                $data['custom_error']   = validation_errors('<li><i class="icon-line-square-check"></i> ', '</li>');
                $view['title']          = 'Konfirmasi Pendaftaran';
                $view['content']        = $this->load->view('complete', $data, TRUE);
            } else {
                
                $this->load->library('password');
                
                $post                   = $this->input->post(NULL, TRUE);
                $cleanPost              = $this->security->xss_clean($post);
                $hashed                 = $this->password->create_hash($cleanPost['reg_pass']);
                $cleanPost['reg_pass']  = $hashed;
                unset($cleanPost['reg_passed']);
                
                $userInfo = $this->auth_model->updateUserInfo($cleanPost);
                if(!$userInfo){
                    $data['custom_error'] = '<li><i class="icon-line-square-check"></i> Terjadi kesalahan. Silahkan ulangi beberapa saat lagi.</li>';
                }
                unset($userInfo->password);
                // foreach($userInfo as $key=>$val){
                //     $this->session->set_userdata($key, $val);
                // }
                // redirect(base_url().'member/login');
                $view['message']    = '<p>Selamat, akun anda telah diaktifkan.<br>Silahkan lakukan proses otentikasi.</p><a href="'.site_url('member/login').'" class="button button-3d button-black">Login</a>.';
                $view['content']    = $this->load->view('submit_success', $view, true);            
                $view['title']      = 'Akun Telah Aktif';
            }
        }
        
        $view['footer_gallery'] = $this->media->images_list(4, 6);
        $this->load->view('master/content', $view);
    }

    public function login()
    {
        //Validation config
        $config = array(
            array(
                'field'     => 'reg_mail',
                'label'     => 'Email',
                'rules'     => 'required|valid_email',
                'errors'    => array(
                    'required'      => 'Mohon isikan alamat email anda.',
                    'valid_email'   => 'Format email belum benar.'
                ),
            ),
            array(
                'field'     => 'reg_pass',
                'label'     => 'Password',
                'rules'     => 'required',
                'errors'    => array(
                    'required'      => 'Mohon isikan password anda.'
                ),
            )
        );

        $this->form_validation->set_rules($config);
        $data['custom_error']   = '';
        
        if($this->form_validation->run() == FALSE) {
            $data['custom_error']   = validation_errors('<li><i class="icon-line-square-check"></i> ', '</li>');
        } else {
            // debug($this->input->post());  
            $post = $this->input->post();
            $clean = $this->security->xss_clean($post);
            $userInfo = $this->auth_model->checkLogin($clean);
            if(!$userInfo){
                $data['custom_error']   = '<li><i class="icon-line-square-check"></i>Upaya masuk anda gagal, silahkan ulangi lagi.</li>';
            }
            foreach($userInfo as $key=>$val){
                $this->session->set_userdata($key, $val);
            }
            redirect(base_url().'member/profile');
            exit;
        }
        
        $view['content']        = $this->load->view('login', $data, TRUE);
        $view['footer_gallery'] = $this->media->images_list(4, 6);
        $view['title']           = 'Halaman Login';
        $this->load->view('master/content', $view);
    }

    public function logout()
    {
        $this->load->library('session');
        $this->session->sess_destroy();
        redirect(site_url('member/login'));
    }
    
    public function profile()
    {
        // security check
        $this->is_logged();

        // get current data
        $query = $this->db->get_where('member', array('member_id'=> $this->session->userdata['member_id']));
        $this->data['prof']     = $query->row();

        // validation rules
        $this->form_validation->set_rules('prof_nama', 'Nama Lengkap', 'required');

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['member_content']   = $this->load->view('biodata', $this->data, true);
            $this->data['content']          = $this->load->view('dashboard', $this->data, true);
        } else {
            // if not break the rules
            $record = array(
                'member_id'     => $this->input->post('member_id'),
                'realname'      => $this->input->post('prof_nama'),
                'birth_city'    => $this->input->post('prof_birth_city'),
                'birth_date'    => $this->input->post('prof_birth_date'),
                'gender'        => $this->input->post('prof_gender'),
                'home_addr'     => $this->input->post('prof_home_addr'),
                'home_city'     => $this->input->post('prof_home_city'),
                'home_prov'     => $this->input->post('prof_home_prov'),
                'phone'         => $this->input->post('prof_phone'),
                'ahli_bid'      => $this->input->post('prof_ahli'),
                'website'       => $this->input->post('prof_web'),
                'socmed_fb'     => $this->input->post('prof_fb'),
                'socmed_tw'     => $this->input->post('prof_tw'),
                'last_update'   => date('Y-m-d H:i:s')
            );

            // start transaction
            $this->db->where('member_id', $this->input->post('member_id'));
            $update  = $this->db->update('member', $record);

            // check for status
            if ($update) {
                $this->data['message']          = 'Your record has been updated.';
                $this->data['button_link']      = 'profile';
                $this->data['content']          = $this->load->view('master/redirect', $this->data, TRUE);
            } else {
                $this->data['member_content']   = $this->load->view('biodata', $this->data, true);
                $this->data['content']          = $this->load->view('dasboard', $this->data, true);
            }
        }

        // make an output
        $this->data['footer_gallery'] = $this->media->images_list(4, 6);
        $this->load->view('master/content', $this->data);        

    }

    public function instansi()
    {
        // security check
        $this->is_logged();

        // get current data
        $query = $this->db->get_where('member', array('member_id'=> $this->session->userdata['member_id']));
        $this->data['prof']     = $query->row();

        // show list output
        $this->db
        ->where('member_id', $this->session->userdata['member_id']);
        $query = $this->db->get('member_office');
        if($query->num_rows() > 0) {
            $this->data['inst']                 = $query->row();
        } else {
            $this->data['inst']                 = new stdClass();
            $this->data['inst']->office_name    = NULL;
            $this->data['inst']->office_addr    = NULL;
            $this->data['inst']->office_city    = NULL;
            $this->data['inst']->office_prov    = NULL;
        }

        // validation rules
        $this->form_validation->set_rules('prof_offc_name', 'Nama Instansi', 'required');

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['member_content']   = $this->load->view('instansi', $this->data, true);
            $this->data['content']          = $this->load->view('dashboard', $this->data, true);
        } else {
            // if not break the rules
            $record = array(
                'member_id'     => $this->input->post('member_id'),
                'office_name'      => $this->input->post('prof_offc_name'),
                'office_addr'    => $this->input->post('prof_offc_addr'),
                'office_city'    => $this->input->post('prof_offc_city'),
                'office_prov'    => $this->input->post('prof_offc_prov'),
                'last_update'   => date('Y-m-d H:i:s')
            );
            // start transaction
            if($query->num_rows() > 0 ) {
                $this->db->where('member_id', $this->input->post('member_id'));
                $update  = $this->db->update('member_office', $record);                
            } else {
                // debug($record);
                $update  = $this->db->insert('member_office', $record);
            }

            // check for status
            if ($update) {
                $this->data['message']          = 'Your record has been updated.';
                $this->data['button_link']      = 'instansi';
                $this->data['content']          = $this->load->view('master/redirect', $this->data, TRUE);
            } else {
                $this->data['member_content']   = $this->load->view('instansi', $this->data, true);
                $this->data['content']          = $this->load->view('dasboard', $this->data, true);
            }
        }

        // make an output
        $this->data['footer_gallery'] = $this->media->images_list(4, 6);
        $this->load->view('master/content', $this->data);        

    }

    public function is_logged() 
    {
        if(empty($this->session->userdata['email'])) {
            redirect(base_url('member/login'));
            exit;            
        }        
    }

}

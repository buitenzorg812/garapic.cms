<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Schedule extends MX_Controller {

    public $data            = array();
    private $url            = 'schedule/index';
    private $perpage        =5;

    public function __construct() 
    {   
        parent::__construct();

        // page title
        $this->data['title'] = 'Agenda Kegiatan';

        // Set default language at beginning
        $this->session->set_userdata(array('lang' => 'id'));

        $this->output->enable_profiler(false);            
    }

    public function index($offset = 0)
    {
        // pagination 
        $this->db
        ->select('a.id')
        ->from('event_lang a')
        ->join('event b', 'b.id = a.id', 'left')
        ->where('a.lang', $this->session->userdata('lang'))
        ->where('b.status', 'publish');
        $query = $this->db->get();
        $count                      = $query->num_rows();
        $config['per_page']         = $this->perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url($this->url), $count, $this->perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        // get all data
        $this->db
        ->select('a.*, b.*')
        ->from('event_lang a')
        ->join('event b', 'b.id = a.id', 'left')
        ->where('b.status', 'publish')
        ->where('a.lang', $this->session->userdata('lang'))
        ->order_by('a.id','desc')
        ->limit($this->perpage, $offset );
        $query = $this->db->get();

        // if not empty
        if($query->num_rows() > 0) {

            // for next event
            $this->db
            ->select('a.*, b.*')
            ->from('event_lang a')
            ->join('event b', 'b.id = a.id', 'left')
            ->where('b.status', 'publish')
            ->where('a.lang', $this->session->userdata('lang'))
            ->where('b.start_date >=', 'CURDATE()')
            ->order_by('a.id','desc')
            ->limit(3);
            $query = $this->db->get();
            $this->data['events'] = $query->result();

            // generate output
            $this->data['schedule']     = $query->result();
            $this->data['content']      = $this->load->view('list', $this->data, true);
        } else {
            // if no data
            $this->data['message']      = '<span>Silahkan kunjungi laman lainnya atau kembali ke <a href="<?php echo base_url() ?>">beranda</a>.</span>';
            $this->data['content']      = $this->load->view('master/blank', $this->data, true);                    
        }


        // Generate output
        $this->data['footer_gallery']   = $this->media->images_list(4, 6); // $cat_id, $limit
        $this->load->view('master/content', $this->data);
    }

    public function read($id)
    {
        $this->db
        ->select('*')
        ->from('event_lang a')
        ->join('event b', 'b.id = a.id', 'left')
        ->where('a.id', $id);
        $query = $this->db->get('');
        $this->data['sch'] = $query->row();

        // for next event
        $this->db
        ->select('a.*, b.*')
        ->from('event_lang a')
        ->join('event b', 'b.id = a.id', 'left')
        ->where('b.status', 'publish')
        ->where('a.lang', $this->session->userdata('lang'))
        ->order_by('a.id','desc')
        ->limit(3);
        $query = $this->db->get();
        $this->data['events'] = $query->result();

        // Generate output
        $this->data['content']          = $this->load->view('read', $this->data, true);
        $this->data['footer_gallery']   = $this->media->images_list(4, 6); // $cat_id, $limit
        $this->load->view('master/content', $this->data);

    }
}

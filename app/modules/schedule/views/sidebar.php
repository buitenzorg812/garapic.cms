<div class="sidebar nobottommargin col_last clearfix">
    <div class="sidebar-widgets-wrap">

        <div class="widget clearfix">

            <h4>Agenda Selanjutnya</h4>
            <div id="post-list-footer">
                
                <?php foreach($events as $event) : ?>
                <div class="spost clearfix">
                    <div class="entry-c">
                        <div class="entry-title">
                            <h4><a href="#"><?php echo $event->title ?></a></h4>
                        </div>
                        <ul class="entry-meta">
                            <li><?php echo mdate('%d %M %Y', strtotime($event->start_date)) ?></li>
                        </ul>
                    </div>
                </div>
                <?php endforeach ?>

            </div>

        </div>


    </div>
</div>
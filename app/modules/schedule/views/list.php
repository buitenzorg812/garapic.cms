<div class="container clearfix topmargin">
    <div class="postcontent nobottommargin">
        <div id="posts" class="events small-thumbs">

            <?php foreach($schedule as $sch) : ?>
            <div class="entry clearfix">
                <div class="entry-image" style="width: 70px !important;">
                    <div>&nbsp;</div>
                    <div class="entry-date"><?php echo mdate('%d', strtotime($sch->start_date)) ?><span><?php echo mdate('%M', strtotime($sch->start_date)) ?></span></div>
                </div>
                <div class="entry-c">
                    <div class="entry-title">
                        <h2><a href="<?php echo site_url('schedule/read/' . $sch->id) ?>"><?php echo $sch->title ?></a></h2>
                    </div>
                    <ul class="entry-meta clearfix">
                        <li><a href="#"><i class="icon-time"></i><?php echo mdate('%d %M %Y', strtotime($sch->start_date)) ?></a></li>
                        <li style="text-overflow: ellipsis; overflow: hidden;"><a href="#"><i class="icon-map-marker2"></i> <?php echo $sch->place ?></a></li>
                    </ul>
                    <div class="entry-content">
                        <p><?php echo word_limiter($sch->content, 20) ?></p>
                    </div>
                </div>
            </div>
            <?php endforeach ?>

        </div>
        <?php echo $pagination ?>                
    </div>
    <?php $this->load->view('sidebar') ?>
</div>



<section id="news-title" class="section notopmargin noborder nobottommargin" >
    <div class="container clearfix">
        <div class="heading-block nobottommargin noborder core-heading-block">
        	<a href="<?php echo site_url('/member/register') ?>" class="button button-border button-rounded fright nomargin">DAFTAR</a>
            <h4><?php echo get_conf('Judul Pendaftaran') ?></h4>
            <span><?php echo get_conf('Info Pendaftaran') ?></span>
        </div>
    </div>
</section>

<section id="section-about" class="page-section">
    <div class="container clearfix">
        <div class="col_one_third nobottommargin">
            <div class="feature-box media-box">
                <div class="fbox-media"><img src="<?php echo base_url(). UPLOADS ?>/p2.jpg" alt="Sekilas Seminar"></div>
                <div class="fbox-desc">
                    <h3>Keanggotaan</h3>
                    <p>Keanggotaan IGI akan memudahkan saling berinteraksi dengan sesama anggota.  </p>
                    <p><a href="<?php echo base_url('read/tentang') ?>" class="more-link">Selengkapnya</a></p>
                </div>
            </div>
        </div>
        <div class="col_one_third nobottommargin">
            <div class="feature-box media-box">
                <div class="fbox-media"><img src="<?php echo base_url(). UPLOADS ?>/p1.jpg" alt="Sekilas Seminar"></div>
                <div class="fbox-desc">
                    <h3>Berita Terbaru</h3>
                    <p>Dapatkan berita-berita terbaru ter-update seputar dunia Geografi. </p>
                    <p><a href="<?php echo base_url('archives') ?>" class="more-link">Selengkapnya</a></p>
                </div>
            </div>
        </div>
        <div class="col_one_third nobottommargin col_last">
            <div class="feature-box media-box">
                <div class="fbox-media"><img src="<?php echo base_url(). UPLOADS ?>/p3.jpg" alt="Sekilas Seminar"></div>
                <div class="fbox-desc">
                    <h3>Berbagi Informasi</h3>
                    <p>Bergabunglah dengan para pakar Geografi dari seluruh Indonesia.</p>
                    <p><a href="<?php echo base_url('read/tentang') ?>" class="more-link">Selengkapnya</a></p>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</section>
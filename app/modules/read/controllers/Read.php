<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Read extends MX_Controller {

    public $data            = array();
    private $perpage        = 1;

    public function __construct() 
    {   
        parent::__construct();

        // page title
        $this->data['title'] = 'Ringkasan';

        // for debug purpose only
        $this->output->enable_profiler(false);
    }

    public function index($slug = '')
    {

        // get all data
        $this->db
        ->select('a.id, a.title, a.body, a.slug, b.created_at, b.media_id, b.updated_at, b.users_id')
        ->from('page_lang a')
        ->join('page b', 'b.id = a.id')
        ->where('a.slug', $slug)
        ->limit($this->perpage);
        $query = $this->db->get();

        // if not empty
        if($query->num_rows() > 0) {

            // most view
            $this->data['view_most']      = $this->media->blog_view_most();
            $this->data['view_latest']    = $this->media->blog_view_latest();

            // galeri
            $this->data['gallery']        = $this->media->images_list(4, 5); // $cat_id, $limit
            $this->data['footer_gallery'] = $this->media->images_list(4, 6); // $cat_id, $limit

            // generate output
            $this->data['blog']         = $query->row();
            $this->data['content']      = $this->load->view('list', $this->data, true);

        } else {
            // if no data
            $this->data['message']      = '<span>Silahkan kunjungi laman lainnya atau kembali ke <a href="<?php echo base_url() ?>">beranda</a>.</span>';
            $this->data['content']      = $this->load->view('master/blank', $this->data, true);                    
        }

        // Generate output
        $this->load->view('master/content', $this->data);
    }

}

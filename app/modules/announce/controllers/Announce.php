<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Announce extends MX_Controller {

    public $data            = array();
    private $url            = 'announce';
    private $perpage        = 5;

    public function __construct() 
    {   
        parent::__construct();

        // page title
        $this->data['title'] = 'Agenda Kegiatan';

        // Set default language at beginning
        $this->session->set_userdata(array('lang' => 'id'));
    }

    public function index($offset = 0)
    {
        // pagination 
        $this->db
        ->select('a.id')
        ->from('page_lang a')
        ->where('b.type', 'announcements')
        ->where('a.lang', $this->session->userdata('lang'))
        ->join('page b', 'b.id = a.id');
        $query = $this->db->get();
        $count                      = $query->num_rows();
        $config['per_page']         = $this->perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url($this->url), $count, $this->perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        // get all data
        $this->db
        ->select('a.id, a.title, a.intro, a.slug, b.created_at, b.media_id, b.updated_at, b.users_id')
        ->from('page_lang a')
        ->where('b.type', 'announcements')
        ->where('a.lang', $this->session->userdata('lang'))
        ->join('page b', 'b.id = a.id')
        ->limit($this->perpage, $offset );
        $query = $this->db->get();

        // if not empty
        if($query->num_rows() > 0) {
            // generate output
            $this->data['blog']         = $query->result();
            $this->data['content']      = $this->load->view('list', $this->data, true);
        } else {
            // if no data
            $this->data['message']      = '<span>Silahkan kunjungi laman lainnya atau kembali ke <a href="<?php echo base_url() ?>">beranda</a>.</span>';
            $this->data['content']      = $this->load->view('master/blank', $this->data, true);                    
        }

        // Generate output
        $this->data['footer_gallery']   = $this->media->images_list(4, 6); // $cat_id, $limit
        $this->load->view('master/content', $this->data);
    }

}

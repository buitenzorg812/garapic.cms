<?php foreach ($Avatar as $img): ?>
						<!-- PHOTO PROFILE BEGIN -->
						<div class="panel panel-default noshadow">
							<div class="panel-heading">
								<h4 class="nobottommargin">Photo Profil</h4>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-md-12">
										<div class="img-profile">
											<img src="<?php if($img->avatar == NULL){ echo base_url('uploads/person').'/person.png'; } else { echo base_url('uploads/person').'/'.$img->avatar; } ?>" />
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- PHOTO PROFILE END -->
<?php endforeach;

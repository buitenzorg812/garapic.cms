<section class="container clearfix">
	<div class="row">
		<div class="panel panel-default noshadow">
			<div class="panel-heading nobottompadding mbrMenu">
				<ul class="nav nav-tabs">
					<li><a href="<?php echo base_url(); ?>dashboard">Dashboard</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/profile">Profile</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/biodata">Biodata</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/instansi">Instansi</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/pendidikan">Pendidikan</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/jabatan">Jabatan</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/publikasi">Publikasi</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/penelitian">Riset</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/pengabdian">Pengabdian</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/logout">Logout</a></li>
				</ul>
			</div>
			<div class="panel-body norightpadding noleftpadding">
				<div class="tab-content">
					<div class="col-md-4 panel nobottommargin">
<?php $this->load->view('avatar'); ?>
					</div>
					<div class="col-md-8 panel nobottommargin">
<?php foreach ($memBio as $bio): ?>
						<!-- FORM PUBLIKASI BEGIN -->
						<?php echo form_open(base_url('dashboard/publikasi'), 'class="form-horizontal nobottommargin" id="biodata-form" name="biodata-form"') ?>
							<?php if($custom_error == '') : ?>
							<h4 class="nomargin">Data Publikasi</h4>
							Mohon lengkapi formulir publikasi anggota berikut ini.
							<hr />
							<?php else: ?>
								<?php $this->load->view('submit_error') ?>
							<?php endif; ?>
							<?php if($success == '') : ?>
							<?php else: ?>
								<?php $this->load->view('submit_success') ?>
							<?php endif; ?>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_pub_title">Judul Publikasi</label>
								<div class="col-md-8">
									<input name="prof_pub_title" value="<?php echo $bio->pub_title; ?>" class="form-control input-md" type="text">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_pub_part">Penulis Lain</label>
								<div class="col-md-8">
									<input name="prof_pub_part" value="<?php echo $bio->pub_partner; ?>" class="form-control input-md" type="text">
									<span class="help-block nobottommargin">Tuliskan nama penulis lain, pisahkan dengan tanda koma.</span>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_pub_type">Jenis Publikasi</label>
								<div class="col-md-8">
									<select name="prof_pub_type" class="form-control">
										<option value="Belum diketahui" style="margin-bottom: 10px;">Pilih Salah Satu</option>
										<option <?php if($bio->pub_type == 'Jurnal'){ echo 'selected = "selected"'; } ?> value="Jurnal">Jurnal</option>
										<option <?php if($bio->pub_type == 'Proceding'){ echo 'selected = "selected"'; } ?> value="Proceding">Proceding</option>
										<option <?php if($bio->pub_type == 'Buku'){ echo 'selected = "selected"'; } ?> value="Buku">Buku</option>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_pub_isbn">ISBN/ISSN</label>
								<div class="col-md-8">
									<input name="prof_pub_isbn" value="<?php echo $bio->pub_isbn; ?>" class="form-control input-md" type="text">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_pub_year">Tahun Terbit</label>
								<div class="col-md-8">
									<input name="prof_pub_year" value="<?php echo $bio->pub_year; ?>" class="form-control input-md" type="text">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_pub_abs">Abstrak Publikasi</label>
								<div class="col-md-8">
									<input name="prof_pub_abs" value="<?php echo $bio->pub_abstract; ?>" class="form-control input-md" type="textarea">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_pub_file">Sample Publikasi</label>
								<div class="col-md-8">
									<div class="input-group">
										<input id="pubfile" type="file" style="display: none;">
										<input id="prof_pub_file" name="prof_pub_file"  class="form-control input-md" type="text">
										<div class="input-group-btn"><a class="btn btn-warning" onclick="$('input[id=pubfile]').click();">Browse</a></div>
									</div>
									<span class="help-block nobottommargin">Pilih dan upload file publikasi anda.</span>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_pub_link">Tautan Publikasi</label>
								<div class="col-md-8">
									<input name="prof_pub_link" value="<?php echo $bio->pub_link; ?>" class="form-control input-md" type="text">
								</div>
							</div>

							<div class="form-group nobottommargin">
								<?php echo form_hidden('member_id', $member_id);?>
								<div class="text-right col-md-12">
									<div id="button1idGroup" class="btn-group" role="group" aria-label="Button Group">
										<button type="submit" class="btn btn-primary nomargin" id="form-submit" name="submit" value="submit">Simpan Data Publikasi</button>
									</div>
								</div>
							</div>
						</form>
						<!-- FORM PUBLIKASI END -->
					</div>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>

<script type="text/javascript">
$('input[id=pubfile]').change(function() {
$('#prof_pub_file').val($(this).val());
});
</script>

<section class="container clearfix">
	<div class="row">
		<div class="panel panel-default noshadow">
			<div class="panel-heading nobottompadding mbrMenu">
				<ul class="nav nav-tabs">
					<li><a href="<?php echo base_url(); ?>dashboard">Dashboard</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/profile">Profile</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/biodata">Biodata</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/instansi">Instansi</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/pendidikan">Pendidikan</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/jabatan">Jabatan</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/publikasi">Publikasi</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/penelitian">Riset</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/pengabdian">Pengabdian</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/logout">Logout</a></li>
				</ul>
			</div>
			<div class="panel-body norightpadding noleftpadding">
				<div class="tab-content">
					<div class="col-md-4 panel nobottommargin">
<?php $this->load->view('avatar'); ?>
					</div>
					<div class="col-md-8 panel nobottommargin">
<?php foreach ($memBio as $bio): ?>
						<!-- FORM PENDIDIKAN BEGIN -->
						<?php echo form_open(base_url('dashboard/pendidikan'), 'class="form-horizontal nobottommargin" id="biodata-form" name="biodata-form"') ?>
							<?php if($custom_error == '') : ?>
							<h4 class="nomargin">Data Pendidikan</h4>
							Mohon lengkapi formulir riwayat pendidikan anggota berikut ini.
							<hr />
							<?php else: ?>
								<?php $this->load->view('submit_error') ?>
							<?php endif; ?>
							<?php if($success == '') : ?>
							<?php else: ?>
								<?php $this->load->view('submit_success') ?>
							<?php endif; ?>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_edu_level">Tingkat Pendidikan</label>
								<div class="col-md-8">
									<select name="prof_edu_level" class="form-control">
										<option value="Belum diketahui" style="margin-bottom: 10px;">Pilih Salah Satu</option>
										<option <?php if($bio->sarjana == 'SMA/SMK'){ echo 'selected = "selected"'; } ?> value="SMA/SMK">SMA/SMK</option>
										<option <?php if($bio->sarjana == 'Diploma'){ echo 'selected = "selected"'; } ?> value="Diploma">Diploma</option>
										<option <?php if($bio->sarjana == 'Sarjana'){ echo 'selected = "selected"'; } ?> value="Sarjana">Sarjana</option>
										<option <?php if($bio->sarjana == 'Magister'){ echo 'selected = "selected"'; } ?> value="Magister">Magister</option>
										<option <?php if($bio->sarjana == 'Doktoral'){ echo 'selected = "selected"'; } ?> value="Doktoral">Doktoral</option>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_edu_jur">Fakultas/Jurusan</label>
								<div class="col-md-8">
									<input name="prof_edu_jur" value="<?php echo $bio->jurusan; ?>" class="form-control input-md" type="text">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_edu_camp">Kampus/Almamater</label>
								<div class="col-md-8">
									<input name="prof_edu_camp" value="<?php echo $bio->kampus; ?>" class="form-control input-md" type="text">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_edu_year">Tahun Lulus</label>
								<div class="col-md-8">
									<input name="prof_edu_year" value="<?php echo $bio->lulus; ?>" class="form-control input-md" type="text">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_edu_title">Judul Makalah</label>
								<div class="col-md-8">
									<input name="prof_edu_title" value="<?php echo $bio->skripsi; ?>" class="form-control input-md" type="text">
									<span class="help-block nobottommargin">Tuliskan judul makalah/skripsi/tesis/disertasi anda.</span>
								</div>
							</div>

							<div class="form-group nobottommargin">
								<?php echo form_hidden('member_id', $member_id);?>
								<div class="text-right col-md-12">
									<div id="button1idGroup" class="btn-group" role="group" aria-label="Button Group">
										<button type="submit" class="btn btn-primary nomargin" id="form-submit" name="submit" value="submit">Simpan</button>
									</div>
								</div>
							</div>
						</form>
						<!-- FORM PENDIDIKAN END -->
					</div>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>

<section class="container clearfix">
	<div class="row">
		<div class="panel panel-default noshadow">
			<div class="panel-heading nobottompadding mbrMenu">
				<ul class="nav nav-tabs">
					<li><a href="<?php echo base_url(); ?>dashboard">Dashboard</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/profile">Profile</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/biodata">Biodata</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/instansi">Instansi</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/pendidikan">Pendidikan</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/jabatan">Jabatan</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/publikasi">Publikasi</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/penelitian">Riset</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/pengabdian">Pengabdian</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/logout">Logout</a></li>
				</ul>
			</div>
			<div class="panel-body norightpadding noleftpadding">
				<div class="tab-content">
					<div class="col-md-4 panel nobottommargin">
<?php $this->load->view('avatar'); ?>
					</div>
					<div class="col-md-8 panel nobottommargin">
<?php foreach ($memBio as $bio): ?>
						<!-- FORM PENELITIAN BEGIN -->
						<?php echo form_open(base_url('dashboard/penelitian'), 'class="form-horizontal nobottommargin" id="biodata-form" name="biodata-form"') ?>
							<?php if($custom_error == '') : ?>
							<h4 class="nomargin">Data Penelitian</h4>
							Mohon lengkapi formulir penelitian anggota berikut ini.
							<hr />
							<?php else: ?>
								<?php $this->load->view('submit_error') ?>
							<?php endif; ?>
							<?php if($success == '') : ?>
							<?php else: ?>
								<?php $this->load->view('submit_success') ?>
							<?php endif; ?>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_riset_title">Judul Penelitian</label>
								<div class="col-md-8">
									<input name="prof_riset_title" value="<?php echo $bio->riset_title; ?>" class="form-control input-md" type="text">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_riset_part">Peneliti Lain</label>
								<div class="col-md-8">
									<input name="prof_riset_part" value="<?php echo $bio->riset_partner; ?>" class="form-control input-md" type="text">
									<span class="help-block nobottommargin">Tuliskan nama peneliti lain, pisahkan dengan tanda koma.</span>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_riset_year">Tahun Penelitian</label>
								<div class="col-md-8">
									<input name="prof_riset_year" value="<?php echo $bio->riset_year; ?>" class="form-control input-md" type="text">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_riset_fund">Sumber Dana</label>
								<div class="col-md-8">
									<input name="prof_riset_fund" value="<?php echo $bio->riset_fund; ?>" class="form-control input-md" type="textarea">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_riset_stake">Stakeholder</label>
								<div class="col-md-8">
									<input name="prof_riset_stake" value="<?php echo $bio->riset_stake; ?>" class="form-control input-md" type="text">
								</div>
							</div>

							<div class="form-group nobottommargin">
								<?php echo form_hidden('member_id', $member_id);?>
								<div class="text-right col-md-12">
									<div id="button1idGroup" class="btn-group" role="group" aria-label="Button Group">
										<button type="submit" class="btn btn-primary nomargin" id="form-submit" name="submit" value="submit">Simpan</button>
									</div>
								</div>
							</div>
						</form>
						<!-- FORM PENELITIAN END -->
					</div>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>

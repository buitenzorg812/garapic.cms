<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Penelitian_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getRiset($data)
	{
		$this->db->select('*');
		$this->db->from('member');
		$this->db->where('email', $data['email']);
		$this->db->join('member_data', 'member.member_id = member_data.member_id', 'left');
		$this->db->join('member_riset', 'member.member_id = member_riset.member_id', 'left');
		$query = $this->db->get();
		if ($query->num_rows() >0){
			foreach ($query->result() as $data) {
				$memBio['member_id'] = $data;
			}
			return $memBio;
		}
		
	}
	
	public function addRiset()
	{
		$biodata = array(
			'member_id'		=> $this->input->post('member_id'),
			'riset_title'	=> $this->input->post('prof_riset_title'),
			'riset_partner'	=> $this->input->post('prof_riset_part'),
			'riset_year'	=> $this->input->post('prof_riset_year'),
			'riset_fund'	=> $this->input->post('prof_riset_fund'),
			'riset_stake'	=> $this->input->post('prof_riset_stake'),
			'last_update'	=> date('Y-m-d H:i:s')
		);
		
		$this->db->select('*');
		$this->db->from('member_riset');
		$query = $this->db->get();
		$result = $query->result_array();
		$count  = count($result);
		
		if (empty($count)) {
			
			$insert = $this->db->insert('member_riset', $biodata);
			$this->db->query($insert);
			return $this->db->insert_id();
			
		} else {

			$insert = $this->db->update('member_riset', $biodata);
			$this->db->query($insert);
			return $this->db->insert_id();
		}
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Biodata_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getBio($data)
	{
		$this->db->select('*');
		$this->db->from('member');
		$this->db->where('email', $data['email']);
		$this->db->join('member_data', 'member.member_id = member_data.member_id', 'left');
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach ($query->result() as $data) {
				$memBio['member_id'] = $data;
			}
			return $memBio;
		}
	}
	
	public function addBio()
	{	
		$nama = array('realname' => $this->input->post('prof_nama'));
		$this->db->update('member', $nama);
		
		$biodata = array(
			'member_id'		=> $this->input->post('member_id'),
			'birth_city'	=> $this->input->post('prof_birth_city'),
			'birth_date'	=> $this->input->post('prof_birth_date'),
			'gender'		=> $this->input->post('prof_gender'),
			'home_addr'		=> $this->input->post('prof_home_addr'),
			'home_city'		=> $this->input->post('prof_home_city'),
			'home_prov'		=> $this->input->post('prof_home_prov'),
			'phone'			=> $this->input->post('prof_phone'),
			'ahli_bid'		=> $this->input->post('prof_ahli'),
			'website'		=> $this->input->post('prof_web'),
			'socmed_fb'		=> $this->input->post('prof_fb'),
			'socmed_tw'		=> $this->input->post('prof_tw'),
			'last_update'	=> date('Y-m-d H:i:s')
		);
		
		$this->db->select('*');
		$this->db->from('member_data');
		$query = $this->db->get();
		$result = $query->result_array();
		$count  = count($result);
		
		if (empty($count)) {
			
			$insert = $this->db->insert('member_data', $biodata);
			$this->db->query($insert);
			return $this->db->insert_id();
			
		} else {

			$insert = $this->db->update('member_data', $biodata);
			$this->db->query($insert);
			return $this->db->insert_id();
		}
	}
}
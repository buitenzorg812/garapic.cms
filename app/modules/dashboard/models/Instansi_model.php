<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Instansi_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getOff($data)
	{
		$this->db->select('*');
		$this->db->from('member');
		$this->db->where('email', $data['email']);
		$this->db->join('member_data', 'member.member_id = member_data.member_id', 'left');
		$this->db->join('member_office', 'member.member_id = member_office.member_id', 'left');
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach ($query->result() as $data) {
				$memBio['member_id'] = $data;
			}
			return $memBio;
		}
	}
	
	public function addOff()
	{
		$biodata = array(
			'member_id'		=> $this->input->post('member_id'),
			'office_name'	=> $this->input->post('prof_offc_name'),
			'office_addr'	=> $this->input->post('prof_offc_addr'),
			'office_city'	=> $this->input->post('prof_offc_city'),
			'office_prov'	=> $this->input->post('prof_offc_prov'),
			'last_update'	=> date('Y-m-d H:i:s')
		);
		
		$this->db->select('*');
		$this->db->from('member_office');
		$query = $this->db->get();
		$result = $query->result_array();
		$count  = count($result);
		
		if (empty($count)) {
			
			$insert = $this->db->insert('member_office', $biodata);
			$this->db->query($insert);
			return $this->db->insert_id();
			
		} else {

			$insert = $this->db->update('member_office', $biodata);
			$this->db->query($insert);
			return $this->db->insert_id();
		}
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		
	}
	
	public function getData($data)
	{
		$this->db->select('*');
		$this->db->from('member');
		$this->db->where('email', $data['email']);
		$this->db->join('member_data', 'member.member_id = member_data.member_id', 'left');
		$this->db->join('member_edu', 'member.member_id = member_edu.member_id', 'left');
		$this->db->join('member_office', 'member.member_id = member_office.member_id', 'left');
		$this->db->join('member_jabatan', 'member.member_id = member_jabatan.member_id', 'left');
		$this->db->join('member_publikasi', 'member.member_id = member_publikasi.member_id', 'left');
		$this->db->join('member_riset', 'member.member_id = member_riset.member_id', 'left');
		$this->db->join('member_abdi', 'member.member_id = member_abdi.member_id', 'left');
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach ($query->result() as $data) {
				$memBio['member_id'] = $data;
			}
			return $memBio;
		}
	}
}
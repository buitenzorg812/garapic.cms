<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model(array('profile_model','crudMod'));
		$this->load->library(array('form_validation','session'));
		$this->load->helper(array('text','date','form','security','string','url'));
	}
	
	public function index()
	{
		if(empty($this->session->userdata['email']))
		{
			redirect(base_url('login'));
			
		} else {
			
			// periksa  http://jsfiddle.net/lesson8/9NeXg/light/
			
			$data				= $this->session->userdata;
			$data['getProf']	= $this->profile_model->getProf($data);
			$data['getJab']		= $this->crudMod->getJab($data);
			
			$validasi = array(
				array(
					'field'			=> 'prof_avatar',
					'label'			=> 'Avatar',
					'rules'			=> 'callback_imgUpload'
				),
			);
			
			$this->form_validation->set_rules($validasi);
			$data['custom_error']	= '';
			$data['success']		= '';
			
			if(isset($_POST['submit'])) {
				if ($this->form_validation->run() == FALSE)
				{
					// If Failed
					$data['custom_error'] 	= validation_errors('<li><i class="icon-line-square-check"></i> ', '</li>');
					
				} else {
					
					// If Success
					$data['success'] = '<li><i class="icon-line-square-check"></i>Photo profil anda berhasil disimpan.</li>';
					redirect(base_url('dashboard'));
				}
			}
			
			
			$view['content'] 		= $this->load->view('profile', $data, TRUE);
			$view['footer_gallery']	= $this->media->images_list(4, 6);
			$view['blog']			= 'Members Private Page';
			$this->load->view('master/full', $view);
		}
	}
	
	public function logout()
	{
		$this->load->library('session');
		$this->session->sess_destroy();
		redirect(base_url().'member/');
	}
	
	// Photo profil validation
	public function imgUpload()
	{
		if($_FILES['prof_avatar']['size'] != 0)
		{
			// If success => check filesize
			if ($_FILES['prof_avatar']['size'] > 500000) // File size in byte.
			{
				// If failed => Error message
				// echo $_FILES['prof_avatar']['size'];
				$data['custom_error'] 	= $this->form_validation->set_message('imgUpload', 'Ukuran berkas photo profil anda melebihi kapasitas.<br />Silahkan unggah berkas photo profil maksimal 500kb.');
				return false;
				
			} else {
				
				// If success => check filetype-mimes
				$imgExt = array('image/jpg', 'image/jpeg', 'image/gif', 'image/png');
				
				if (!in_array($_FILES['prof_avatar']['type'], $imgExt))
				{
					// If failed => Error message
					// echo $_FILES['prof_avatar']['type'];
					$data['custom_error'] 	= $this->form_validation->set_message('imgUpload', '<li><i class="icon-line-square-check"></i> Mohon maaf format berkas anda tidak didukung.<br />Silahkan unggah berkas berformat <strong>JPG, PNG atau GIF</strong>.</li>');
					return false;
					
				} else {
					
					// If success => check file dimensions
					$imgAva	= getimagesize($_FILES['prof_avatar']['tmp_name']);
					$imgH	= $imgAva[0];
					$imgW	= $imgAva[1];
					if ($imgH != 240 && $imgW != 240)
					{
						// If failed => Error message
						// echo $imgH.'px x '.$imgW.'px';
						$data['custom_error'] 	= $this->form_validation->set_message('imgUpload', '<li><i class="icon-line-square-check"></i> Dimensi berkas photo profil anda harus <strong>240px x 240px</strong>.</li>');
						return false;
						
					} else {
						
						// If success => do_upload
						$imageName	= md5(time());
						$avatar = array(
							'allowed_types'	=> 'jpg|jpeg|png|gif',
							'upload_path'	=> 'uploads/person',
							'file_name'		=> $imageName,
							'max_height'	=> '240',
							'max_width'		=> '240',
							'min_height'	=> '240',
							'min_width'		=> '240',
							'max_size'		=> '500'
						);

						$this->load->library('upload', $avatar);						
						
						if (!$this->upload->do_upload('prof_avatar'))
						{
							// If failed => Error message
							$data['custom_error'] 	= $this->form_validation->set_message('imgUpload', '<li><i class="icon-line-square-check"></i> Unggah berkas gagal.</li>');
							return false;
							
						} else {
							
							$object = $this->upload->data();
							$imgUse	= $object['raw_name'].''.$object['file_ext'];

							$biodata = array(
								'member_id'		=> $this->input->post('member_id'),
								'avatar'		=> $imgUse,
								'last_update'	=> date('Y-m-d H:i:s')
							);
							
							$this->profile_model->addPhoto($biodata);
						}
					}
				}
			}
		} else {
			
			// If failed => Error message => File not selected
			$data['custom_error'] 	= $this->form_validation->set_message('imgUpload', 'Mohon pilih berkas photo profil anda.');
			return false;
		}
	}
}
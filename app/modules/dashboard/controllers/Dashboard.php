<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('crudMod');
		$this->load->library(array('form_validation','session'));
		$this->load->helper(array('text','date','form','security','string','url'));
	}
	
	public function index()
	{
		if(empty($this->session->userdata['email']))
		{
			redirect(base_url('login'));
		} else {
		
			$data				= $this->session->userdata;
			$data['Avatar']		= $this->crudMod->getAvatar($data);
			$data['getData']	= $this->crudMod->getData($data);
			
		}
		$view['content'] 		= $this->load->view('dashboard', $data, TRUE);
		$view['footer_gallery']	= $this->media->images_list(4, 6);
		$view['blog']			= 'Members Private Page';
		$this->load->view('master/full', $view);
	}
	
	public function logout()
	{
		$this->load->library('session');
		$this->session->sess_destroy();
		redirect(base_url().'member/');
	}
}
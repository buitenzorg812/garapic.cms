<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Penelitian extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model(array('penelitian_model', 'crudMod'));
		$this->load->library(array('form_validation','session'));
		$this->load->helper(array('text','date','form','security','string','url'));
	}
	
	public function index()
	{
		if(empty($this->session->userdata['email']))
		{
			redirect(base_url('login'));
		} else {
			
			$data = $this->session->userdata;
			$data['Avatar'] = $this->crudMod->getAvatar($data);
			$data['memBio'] = $this->penelitian_model->getRiset($data);
			
			$validasi = array(
				array(
					'field'			=> 'prof_riset_title',
					'label'			=> 'Judul Penelitian',
					'rules'			=> 'required',
					'errors'		=> array(
						'required' 	=> 'Mohon isikan judul penelitian anda.'
					),
				),
				array(
					'field'			=> 'prof_riset_year',
					'label'			=> 'Tahun Penelitian',
					'rules'			=> 'required|numeric|min_length[4]|max_length[4]',
					'errors'		=> array(
						'required' 	=> 'Mohon isikan tahun penelitian anda.',
						'numeric'	=> 'Mohon isikan tahun penelitian hanya dengan angka.',
						'min_length'=> 'Penulisan tahun harus 4 angka.',
						'max_length'=> 'Penulisan tahun harus 4 angka.'
					),
				),
			);
			
			$this->form_validation->set_rules($validasi);
			$data['custom_error']	= '';
			$data['success']		= '';
			
			if(isset($_POST['submit'])) {
			
				if ($this->form_validation->run() == FALSE)
				{
					// If Failed
					$data['custom_error'] 	= validation_errors('<li><i class="icon-line-square-check"></i> ', '</li>');

				} else {
					
					// If Success
					$clean = $this->security->xss_clean($this->input->post(NULL, TRUE));
					$this->penelitian_model->addRiset($clean);
					$data['success'] = '<li><i class="icon-line-square-check"></i>Data anda berhasil disimpan.</li>';
					redirect(base_url('dashboard/profile'));
				}
			}
		
			$view['content'] 		= $this->load->view('penelitian', $data, TRUE);
			$view['footer_gallery']	= $this->media->images_list(4, 6);
			$view['blog']			= 'Members Private Page';
			$this->load->view('master/full', $view);
		}
	}
	
	public function logout()
	{
		$this->load->library('session');
		$this->session->sess_destroy();
		redirect(base_url().'member/');
	}
}

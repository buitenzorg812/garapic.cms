<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pendidikan extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('pendidikan_model', 'crudMod'));
		$this->load->library(array('form_validation','session'));
		$this->load->helper(array('text','date','form','security','string','url'));
	}
	
	public function index()
	{
		if(empty($this->session->userdata['email']))
		{
			redirect(base_url('login'));
		} else {
			
			$data = $this->session->userdata;
			$data['Avatar'] = $this->crudMod->getAvatar($data);
			$data['memBio'] = $this->pendidikan_model->getEdu($data);
			
			$validasi = array(
				array(
					'field'			=> 'prof_edu_level',
					'label'			=> 'Tingkat Pendidikan',
					'rules'			=> 'required',
					'errors'		=> array(
						'required' 	=> 'Mohon isikan tingkat pendidikan anda.'
					),
				),
				array(
					'field'			=> 'prof_edu_jur',
					'label'			=> 'Fakultas/Jurusan',
					'rules'			=> 'required',
					'errors'		=> array(
						'required' 	=> 'Mohon isikan fakultas/jurusan anda.'
					),
				),
				array(
					'field'			=> 'prof_edu_camp',
					'label'			=> 'Kampus/Almamater',
					'rules'			=> 'required',
					'errors'		=> array(
						'required' 	=> 'Mohon isikan kampus/almamater anda.'
					),
				),
				array(
					'field'			=> 'prof_edu_year',
					'label'			=> 'Tahun Lulus',
					'rules'			=> 'required|numeric|min_length[4]|max_length[4]',
					'errors'		=> array(
						'required' 	=> 'Mohon isikan tahun lulus anda.',
						'numeric'	=> 'Mohon isikan tahun lulus hanya dengan angka.',
						'min_length'=> 'Penulisan tahun harus 4 angka.',
						'max_length'=> 'Penulisan tahun harus 4 angka.'
					),
				),
				array(
					'field'			=> 'prof_edu_title',
					'label'			=> 'Judul Makalah',
					'rules'			=> 'required',
					'errors'		=> array(
						'required' 	=> 'Mohon isikan judul makalah anda.'
					),
				),
			);
			
			$this->form_validation->set_rules($validasi);
			$data['custom_error']	= '';
			$data['success']		= '';
			
			if(isset($_POST['submit'])) {
			
				if ($this->form_validation->run() == FALSE)
				{
					// If Failed
					$data['custom_error'] 	= validation_errors('<li><i class="icon-line-square-check"></i> ', '</li>');

				} else {
					
					// If Success
					$clean = $this->security->xss_clean($this->input->post(NULL, TRUE));
					$this->pendidikan_model->addEdu($clean);
					$data['success'] = '<li><i class="icon-line-square-check"></i>Data anda berhasil disimpan.</li>';
					redirect(base_url('dashboard/profile'));
				}
			}
		
			$view['content'] 		= $this->load->view('pendidikan', $data, TRUE);
			$view['footer_gallery']	= $this->media->images_list(4, 6);
			$view['blog']			= 'Members Private Page';
			$this->load->view('master/full', $view);
		}
	}
	
	public function logout()
	{
		$this->load->library('session');
		$this->session->sess_destroy();
		redirect(base_url().'member/');
	}
}

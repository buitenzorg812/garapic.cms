<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['status']					= array('pending', 'approved');

// Sessions
$config['sess_use_database']		= TRUE;
$config['sess_driver']				= 'database';
$config['sess_table_name']			= 'member_sessions';
$config['sess_save_path']			= 'member_sessions';
$config['sess_match_ip']			= FALSE;
$config['sess_expiration']			= 7200;
$config['sess_time_to_update']		= 3600;
$config['sess_regenerate_destroy']	= FALSE;
<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/* load the MX_Router class */
require_once APPPATH .'third_party/PhpWord/Autoloader.php';
\PhpOffice\PhpWord\Autoloader::register();

class Word extends \PhpOffice\PhpWord\PhpWord {
	public function __construct() 
	{	
		parent::__construct();		
	}	
}
